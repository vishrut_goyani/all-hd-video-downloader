package all.hd.downloader.videodownloader.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import all.hd.downloader.videodownloader.Activity.Video_View_Activity;
import all.hd.downloader.videodownloader.Activity.View_Image_Activity;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.Interfaces.Instagram_Story_Click;
import all.hd.downloader.videodownloader.models.story_list.ItemModel;

import all.hd.downloader.videodownloader.R;

import java.io.File;
import java.util.ArrayList;

public class StoriesListAdapter extends RecyclerView.Adapter<StoriesListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ItemModel> storyItemModelList;
    Instagram_Story_Click instagram_story_click;

    public StoriesListAdapter(Context context2, ArrayList<ItemModel> arrayList, Instagram_Story_Click instagram_story_click) {
        this.context = context2;
        this.storyItemModelList = arrayList;
        this.instagram_story_click = instagram_story_click;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.items_whatsapp_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final ItemModel itemModel = this.storyItemModelList.get(i);
        try {
            if (itemModel.getMedia_type() == 2) {
                viewHolder.ivPlay.setVisibility(View.VISIBLE);
            } else {
                viewHolder.ivPlay.setVisibility(View.GONE);
            }
            Glide.with(this.context).load(itemModel.getImage_versions2().getCandidates().get(0).getUrl()).into(viewHolder.pcw);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (itemModel.getMedia_type() == 2) {
            File file = new File(AppUtils.RootDirectoryInstaShow, "story_" + storyItemModelList.get(i).getId() + ".mp4");
            if (file.exists()) {
                viewHolder.tv_download.setText("Downloaded");
            } else {
                viewHolder.tv_download.setText("Download");
            }
        } else {
            File file = new File(AppUtils.RootDirectoryInstaShow, "story_" + storyItemModelList.get(i).getId() + ".png");
            if (file.exists()) {
                viewHolder.tv_download.setText("Downloaded");
            } else {
                viewHolder.tv_download.setText("Download");
            }
        }
        viewHolder.itemView.setOnClickListener(view -> {
            if (storyItemModelList.get(i).getMedia_type() == 2) {
                Intent intent = new Intent(context, Video_View_Activity.class);
                intent.putExtra("path", storyItemModelList.get(i).getVideo_versions().get(0).getUrl());
                context.startActivity(intent);
            } else {
                Intent intent = new Intent(context, View_Image_Activity.class);
                intent.putExtra("path", itemModel.getImage_versions2().getCandidates().get(0).getUrl());
                context.startActivity(intent);
            }
        });
        viewHolder.tv_download.setOnClickListener(v -> instagram_story_click.clickInsta(i));
    }

    @Override
    public int getItemCount() {
        ArrayList<ItemModel> arrayList = this.storyItemModelList;
        if (arrayList == null) {
            return 0;
        }
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pcw, ivPlay;
        TextView tv_download;

        public ViewHolder(View itemsWhatsappViewBinding) {
            super(itemsWhatsappViewBinding);
            tv_download = itemsWhatsappViewBinding.findViewById(R.id.tv_download);
            pcw = itemsWhatsappViewBinding.findViewById(R.id.pcw);
            ivPlay = itemsWhatsappViewBinding.findViewById(R.id.iv_play);
            itemView.findViewById(R.id.popup).setVisibility(View.GONE);
        }
    }
}
