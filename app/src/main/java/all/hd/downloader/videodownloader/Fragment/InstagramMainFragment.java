package all.hd.downloader.videodownloader.Fragment;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import all.hd.downloader.videodownloader.Activity.InstaLoginActivity;
import all.hd.downloader.videodownloader.Adapter.StoriesListAdapter;
import all.hd.downloader.videodownloader.Adapter.UserListAdapter;
import all.hd.downloader.videodownloader.Api.CommonClassForAPI;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.App_Main.SharePrefs;
import all.hd.downloader.videodownloader.Interfaces.Instagram_Story_Click;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.models.Download_Data;
import all.hd.downloader.videodownloader.models.Edge;
import all.hd.downloader.videodownloader.models.EdgeSidecarToChildren;
import all.hd.downloader.videodownloader.models.ResponseModel;
import all.hd.downloader.videodownloader.models.story_list.FullDetailModel;
import all.hd.downloader.videodownloader.models.story_list.ItemModel;
import all.hd.downloader.videodownloader.models.story_list.StoryModel;
import all.hd.downloader.videodownloader.models.story_list.TrayModel;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;
import all.hd.downloader.videodownloader.widgets.SliderLayoutManager;
import io.reactivex.observers.DisposableObserver;

public class InstagramMainFragment extends Fragment implements Instagram_Story_Click {
    private String PhotoUrl = "";
    private String VideoUrl = "";
    private ClipboardManager clipBoard;
    CommonClassForAPI commonClassForAPI;
    Switch SwitchLogin;
    EditText et_text;
    MaterialButton btnLogin;
    TextView tvViewStories;
    MaterialButton login_btn1, tv_paste;
    ProgressBar pr_loading_bar;
    RelativeLayout RLLoginInstagram;
    MaterialButton LLOpenInstagram;
    RecyclerView RVStories;
    RecyclerView RVUserList;
    ArrayList<Download_Data> downloadDataArrayList = new ArrayList<>();
    private ArrayList<ItemModel> storyItemModelList = new ArrayList<>();
    ArrayList<TrayModel> storyModels = new ArrayList<>();
    Context activity;
    public StoriesListAdapter storiesListAdapter;
    ArrayList<String> paths = new ArrayList<>();

    private UnifiedNativeAd mNativeAdDialog;
    View viewDialog;
    FrameLayout adFrame;
    AlertDialog.Builder builder;
    RoundedHorizontalProgressBar rh_progress_bar;
    TextView txtPerc;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = context;
    }

    private DisposableObserver<JsonObject> instaObserver = new DisposableObserver<JsonObject>() {
        @Override
        public void onNext(JsonObject jsonObject) {
            try {
                ResponseModel responseModel = new Gson().fromJson(jsonObject.toString(), new TypeToken<ResponseModel>() {
                }.getType());
                EdgeSidecarToChildren edge_sidecar_to_children = responseModel.getGraphql().getShortcode_media().getEdge_sidecar_to_children();
                AppUtils.hideProgressDialog(getActivity());
                if (edge_sidecar_to_children != null) {
                    List<Edge> edges = edge_sidecar_to_children.getEdges();
                    for (int i = 0; i < edges.size(); i++) {
                        if (edges.get(i).getNode().isIs_video()) {
                            InstagramMainFragment.this.VideoUrl = edges.get(i).getNode().getVideo_url();
                            Download_Data download_data = new Download_Data();
                            download_data.setUrl(VideoUrl);
                            download_data.setName(getVideoFilenameFromURL(VideoUrl));
                            downloadDataArrayList.add(download_data);
                        } else {
                            InstagramMainFragment.this.PhotoUrl = edges.get(i).getNode().getDisplay_resources().get(edges.get(i).getNode().getDisplay_resources().size() - 1).getSrc();
                            Download_Data download_data = new Download_Data();
                            download_data.setUrl(PhotoUrl);
                            download_data.setName(getImageFilenameFromURL(PhotoUrl));
                            downloadDataArrayList.add(download_data);
                        }
                    }
                    AppUtils.hideProgressDialog(getActivity());
                    if (!VideoUrl.equals("")) {
                        File file = new File(AppUtils.RootDirectoryWhatsappShow, getVideoFilenameFromURL(VideoUrl));
                        if (!file.exists()) {
                            paths = new ArrayList<>();
                            downloadDataArrayList = new ArrayList<>();
                            new DownloadImageMultiple().execute(VideoUrl, getVideoFilenameFromURL(VideoUrl));
//                            downloadMultiple();
                        } else {
                            Toast.makeText(activity, "Already Downloaded", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        File file = new File(AppUtils.RootDirectoryWhatsappShow, getImageFilenameFromURL(PhotoUrl));
                        if (!file.exists()) {
                            paths = new ArrayList<>();
                            new DownloadImageMultiple().execute(PhotoUrl, getImageFilenameFromURL(PhotoUrl));
                        } else {
                            Toast.makeText(activity, "Already Downloaded", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (responseModel.getGraphql().getShortcode_media().isIs_video()) {
                    InstagramMainFragment.this.VideoUrl = responseModel.getGraphql().getShortcode_media().getVideo_url();
                    Download_Data download_data = new Download_Data();
                    download_data.setUrl(VideoUrl);
                    download_data.setName(getVideoFilenameFromURL(VideoUrl));
                    downloadDataArrayList.add(download_data);
                    File file = new File(AppUtils.RootDirectoryWhatsappShow, getVideoFilenameFromURL(VideoUrl));
                    if (!file.exists()) {
                        new DownloadSingleImage().execute(VideoUrl, getVideoFilenameFromURL(VideoUrl));
                    } else {
                        Toast.makeText(activity, "Already Downloaded", Toast.LENGTH_SHORT).show();
                    }
                    InstagramMainFragment.this.VideoUrl = "";
                    et_text.setText("");
                } else {
                    InstagramMainFragment.this.PhotoUrl = responseModel.getGraphql().getShortcode_media().getDisplay_resources().get(responseModel.getGraphql().getShortcode_media().getDisplay_resources().size() - 1).getSrc();
                    Download_Data download_data = new Download_Data();
                    download_data.setUrl(PhotoUrl);
                    download_data.setName(getImageFilenameFromURL(PhotoUrl));
                    downloadDataArrayList.add(download_data);

                    File file = new File(AppUtils.RootDirectoryWhatsappShow, getImageFilenameFromURL(PhotoUrl));
                    if (!file.exists()) {
                        new DownloadSingleImage().execute(PhotoUrl, getImageFilenameFromURL(PhotoUrl));
                    } else {
                        Toast.makeText(activity, "Already Downloaded", Toast.LENGTH_SHORT).show();
                    }
                    InstagramMainFragment.this.PhotoUrl = "";
                    et_text.setText("");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(Throwable th) {
            AppUtils.hideProgressDialog(getActivity());
            th.printStackTrace();
        }

        @Override
        public void onComplete() {
            AppUtils.hideProgressDialog(getActivity());
        }
    };

    private int downloadId;
//    private void downloadMultiple() {
//        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
//                .setDatabaseEnabled(true)
//                .build();
//        PRDownloader.initialize(activity, config);
//        downloadId = PRDownloader.download(VideoUrl, AppUtils.RootDirectoryInstaShow, _name + _end)
//                .build()
//                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
//                    @Override
//                    public void onStartOrResume() {
//
//                    }
//                })
//                .setOnProgressListener(
//                        progress -> {
//                            long progressPercent = progress.currentBytes * 100 / progress.totalBytes;
//                            progressbar1.setProgress((int) progressPercent);
//                            textview1.setText(Utilss.getProgressDisplayLine(progress.currentBytes, progress.totalBytes));
//                        })
//                .start(new OnDownloadListener() {
//                    @Override
//                    public void onDownloadComplete() {
//
//                    }
//
//                    @Override
//                    public void onError(Error error) {
//
//                    }
//                });
//    }

    public static final class Utilss {
        private Utilss() {
        }

        public static String getProgressDisplayLine(long currentBytes, long totalBytes) {
            return getBytesToMBString(currentBytes) + "/" + getBytesToMBString(totalBytes);
        }

        private static String getBytesToMBString(long bytes) {
            return String.format(Locale.ENGLISH, "%.2fMb", bytes / (1024.00 * 1024.00));
        }
    }

    private final DisposableObserver<FullDetailModel> storyDetailObserver = new DisposableObserver<FullDetailModel>() {
        @Override
        public void onNext(FullDetailModel fullDetailModel) {
            RVUserList.setVisibility(View.VISIBLE);
            pr_loading_bar.setVisibility(View.GONE);
            try {
                storyItemModelList = fullDetailModel.getReel_feed().getItems();
                storiesListAdapter = new StoriesListAdapter(activity, storyItemModelList, InstagramMainFragment.this);
                RVStories.setAdapter(storiesListAdapter);
                storiesListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(Throwable th) {
            pr_loading_bar.setVisibility(View.GONE);
            th.printStackTrace();
        }

        @Override
        public void onComplete() {
            pr_loading_bar.setVisibility(View.GONE);
        }
    };
    private final DisposableObserver<StoryModel> storyObserver = new DisposableObserver<StoryModel>() {
        @Override
        public void onNext(@NotNull StoryModel storyModel) {
            RVUserList.setVisibility(View.VISIBLE);
            pr_loading_bar.setVisibility(View.GONE);
            try {
                storyModels = new ArrayList<>();
                storyModels = storyModel.getTray();

                SliderLayoutManager sliderLayoutManager = new SliderLayoutManager(getActivity());
                sliderLayoutManager.setCallback(layoutPosition -> {
                    Log.e("TAG", "onNext: " + layoutPosition);
                });
                RVUserList.setLayoutManager(sliderLayoutManager);
                RVUserList.setNestedScrollingEnabled(false);
                new LinearSnapHelper().attachToRecyclerView(RVUserList);
                int padding = getScreenWidth(getActivity()) / 2 - dpToPx(getActivity(), 32);
                RVUserList.setPadding(padding, 0, padding, 0);

                for (int i = storyModels.size() - 1; i >= 0; i--) {
                    if (storyModels.get(i).getUser() == null)
                        storyModels.remove(i);
                }
                UserListAdapter userListAdapter = new UserListAdapter(activity, storyModels);
                userListAdapter.setClick((layoutPosition, trayModel) -> {
                    RVUserList.smoothScrollToPosition(layoutPosition);
                    callStoriesDetailApi(String.valueOf(storyModels.get(layoutPosition).getUser().getPk()));
                });
                RVUserList.setAdapter(userListAdapter);
                RVUserList.smoothScrollToPosition(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onError(Throwable th) {
            pr_loading_bar.setVisibility(View.GONE);
            th.printStackTrace();
        }

        @Override
        public void onComplete() {
            pr_loading_bar.setVisibility(View.GONE);
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_instagram_main, container, false);
        commonClassForAPI = CommonClassForAPI.getInstance(getActivity());
        init(view);
        AppUtils.createFileFolder();
        initViews();
        if (adFrame != null)
            if (PreferenceUtility.getInstance(activity).getAdmobNative().equals("yes"))
                refreshDialogAd(adFrame);
        return view;
    }

    private void init(View view) {
        if (viewDialog == null)
            viewDialog = LayoutInflater.from(activity).inflate(R.layout.download_dialog, null);
        adFrame = viewDialog.findViewById(R.id.adFrame);
        rh_progress_bar = viewDialog.findViewById(R.id.rh_progress_bar);
        txtPerc = viewDialog.findViewById(R.id.txtPerc);

        LLOpenInstagram = view.findViewById(R.id.LLOpenInstagram);
        RLLoginInstagram = view.findViewById(R.id.RLLoginInstagram);
        SwitchLogin = view.findViewById(R.id.SwitchLogin);
        et_text = view.findViewById(R.id.et_text);
        tv_paste = view.findViewById(R.id.tv_paste);
        login_btn1 = view.findViewById(R.id.login_btn1);
        tvViewStories = view.findViewById(R.id.tvViewStories);
        btnLogin = view.findViewById(R.id.tvLogin);
        RVUserList = view.findViewById(R.id.RVUserList);
        pr_loading_bar = view.findViewById(R.id.pr_loading_bar);
        RVStories = view.findViewById(R.id.RVStories);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.clipBoard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        new Handler().postDelayed(() -> PasteText(), 200);
    }

    private void initViews() {
        this.clipBoard = (ClipboardManager) this.activity.getSystemService(Context.CLIPBOARD_SERVICE);

        login_btn1.setOnClickListener(view -> InstagramMainFragment.this.initViews$0$InstagramActivity(view));
        tv_paste.setOnClickListener(view -> InstagramMainFragment.this.initViews$1$InstagramActivity(view));
        LLOpenInstagram.setOnClickListener(view -> InstagramMainFragment.this.initViews2InstagramActivity(view));

        GridLayoutManager gridLayoutManager2 = new GridLayoutManager(activity, 3);
        RVStories.setLayoutManager(gridLayoutManager2);
        RVStories.setNestedScrollingEnabled(false);
        gridLayoutManager2.setOrientation(RecyclerView.VERTICAL);

        if (SharePrefs.getInstance(this.activity).getBoolean(SharePrefs.ISINSTALOGIN)) {
            layoutCondition();
            callStoriesApi();
            SwitchLogin.setChecked(true);
        } else {
            SwitchLogin.setChecked(false);
        }
        btnLogin.setOnClickListener(view -> startActivityForResult(new Intent(getActivity(), InstaLoginActivity.class), 100));
        RLLoginInstagram.setOnClickListener(view -> {
            if (!SharePrefs.getInstance(getActivity()).getBoolean(SharePrefs.ISINSTALOGIN)) {
                InstagramMainFragment.this.startActivityForResult(new Intent(getActivity(), InstaLoginActivity.class), 100);
                return;
            }
            builder = new AlertDialog.Builder(getActivity());
            builder.setPositiveButton(getResources().getString(R.string.yes), (dialogInterface, i) -> {
                SharePrefs.getInstance(getActivity()).putBoolean(SharePrefs.ISINSTALOGIN, false);
                SharePrefs.getInstance(getActivity()).putString(SharePrefs.COOKIES, "");
                SharePrefs.getInstance(getActivity()).putString(SharePrefs.CSRF, "");
                SharePrefs.getInstance(getActivity()).putString(SharePrefs.SESSIONID, "");
                SharePrefs.getInstance(getActivity()).putString(SharePrefs.USERID, "");
                if (SharePrefs.getInstance(getActivity()).getBoolean(SharePrefs.ISINSTALOGIN).booleanValue()) {
                    SwitchLogin.setChecked(true);
                } else {
                    SwitchLogin.setChecked(false);
                    RVUserList.setVisibility(View.GONE);
                    RVStories.setVisibility(View.GONE);
                    tvViewStories.setText(getActivity().getResources().getText(R.string.view_stories));
                    btnLogin.setVisibility(View.VISIBLE);
                }
                dialogInterface.cancel();
            });
            builder.setNegativeButton(InstagramMainFragment.this.getResources().getString(R.string.cancel), (dialogInterface, i) -> dialogInterface.cancel());
            AlertDialog create = builder.create();
            create.setTitle(InstagramMainFragment.this.getResources().getString(R.string.do_u_want_to_download_media_from_pvt));
            create.show();
        });
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void initViews$0$InstagramActivity(View view) {
        String obj = et_text.getText().toString();
        if (obj.equals("")) {
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_url));
        } else if (!Patterns.WEB_URL.matcher(obj).matches()) {
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_valid_url));
        } else {
            GetInstagramData();
        }
    }

    public void initViews$1$InstagramActivity(View view) {
        PasteText();
    }

    public void initViews2InstagramActivity(View view) {
        AppUtils.OpenApp(this.activity, "com.instagram.android");
    }

    public void layoutCondition() {
        tvViewStories.setText(this.activity.getResources().getString(R.string.stories));
        btnLogin.setVisibility(View.GONE);
    }

    private void GetInstagramData() {
        try {
            AppUtils.createFileFolder();
            String host = new URL(et_text.getText().toString()).getHost();
            if (host.equals("www.instagram.com")) {
                callDownload(et_text.getText().toString());
            } else {
                AppUtils.setToast(this.activity, getResources().getString(R.string.enter_valid_url));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PasteText() {
        try {
            et_text.setText("");
            String stringExtra = getActivity().getIntent().getStringExtra("CopyIntent");
            if (stringExtra.equals("")) {
                if (this.clipBoard.hasPrimaryClip()) {
                    if (this.clipBoard.getPrimaryClipDescription().hasMimeType("text/plain")) {
                        ClipData.Item itemAt = this.clipBoard.getPrimaryClip().getItemAt(0);
                        if (itemAt.getText().toString().contains("instagram.com")) {
                            et_text.setText(itemAt.getText().toString());
                        }
                    } else if (this.clipBoard.getPrimaryClip().getItemAt(0).getText().toString().contains("instagram.com")) {
                        et_text.setText(this.clipBoard.getPrimaryClip().getItemAt(0).getText().toString());
                    }
                }
            } else if (stringExtra.contains("instagram.com")) {
                et_text.setText(stringExtra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getUrlWithoutParameters(String str) {
        try {
            URI uri = new URI(str);
            return new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), null, uri.getFragment()).toString();
        } catch (Exception e) {
            e.printStackTrace();
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_valid_url));
            return "";
        }
    }

    private void callDownload(String str) {
        String str2 = getUrlWithoutParameters(str) + "?__a=1";
        try {
            if (!new AppUtils(this.activity).isNetworkAvailable(getContext())) {
                AppUtils.setToast(this.activity, getResources().getString(R.string.no_net_conn));
            } else if (this.commonClassForAPI != null) {
                AppUtils.showProgressDialog(getActivity());
                this.commonClassForAPI.callResult(this.instaObserver, str2, "ds_user_id=" + SharePrefs.getInstance(this.activity).getString(SharePrefs.USERID) + "; sessionid=" + SharePrefs.getInstance(this.activity).getString(SharePrefs.SESSIONID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getImageFilenameFromURL(String str) {
        try {
            return new File(new URL(str).getPath().toString()).getName();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".png";
        }
    }

    public String getVideoFilenameFromURL(String str) {
        try {
            return new File(new URL(str).getPath().toString()).getName();
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".mp4";
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.instaObserver.dispose();
    }

    @Override
    public void onActivityResult(int i, int i2, Intent intent) {
        try {
            super.onActivityResult(i, i2, intent);
            if (i == 100 && i2 == -1) {
                intent.getStringExtra("key");
                if (SharePrefs.getInstance(this.activity).getBoolean(SharePrefs.ISINSTALOGIN).booleanValue()) {
                    SwitchLogin.setChecked(true);
                    layoutCondition();
                    callStoriesApi();
                    return;
                }
                SwitchLogin.setChecked(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLocale(String str) {
        Locale locale = new Locale(str);
        Resources resources = getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        resources.updateConfiguration(configuration, displayMetrics);
    }

    private void callStoriesApi() {
        try {
            if (!new AppUtils(this.activity).isNetworkAvailable(getContext())) {
                AppUtils.setToast(activity, getResources().getString(R.string.no_net_conn));
            } else if (this.commonClassForAPI != null) {
                pr_loading_bar.setVisibility(View.VISIBLE);
                CommonClassForAPI commonClassForAPI2 = this.commonClassForAPI;
                DisposableObserver<StoryModel> disposableObserver = this.storyObserver;
                commonClassForAPI2.getStories(disposableObserver, "ds_user_id=" + SharePrefs.getInstance(this.activity).getString(SharePrefs.USERID) + "; sessionid=" + SharePrefs.getInstance(this.activity).getString(SharePrefs.SESSIONID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callStoriesDetailApi(String str) {
        try {
            if (!new AppUtils(this.activity).isNetworkAvailable(getContext())) {
                AppUtils.setToast(activity, getResources().getString(R.string.no_net_conn));
            } else if (this.commonClassForAPI != null) {
                pr_loading_bar.setVisibility(View.VISIBLE);
                CommonClassForAPI commonClassForAPI2 = this.commonClassForAPI;
                DisposableObserver<FullDetailModel> disposableObserver = this.storyDetailObserver;
                commonClassForAPI2.getFullDetailFeed(disposableObserver, str, "ds_user_id=" + SharePrefs.getInstance(this.activity).getString(SharePrefs.USERID) + "; sessionid=" + SharePrefs.getInstance(this.activity).getString(SharePrefs.SESSIONID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clickInsta(int pos) {
        if (storyItemModelList.get(pos).getMedia_type() == 2) {
            String url = storyItemModelList.get(pos).getVideo_versions().get(0).getUrl();
            new DownloadSingleImage().execute(url, "story_" + storyItemModelList.get(pos).getId() + ".mp4");
            return;
        }
        String url2 = storyItemModelList.get(pos).getImage_versions2().getCandidates().get(0).getUrl();
        new DownloadSingleImage().execute(url2, "story_" + storyItemModelList.get(pos).getId() + ".png");
    }


    class DownloadImageMultiple extends AsyncTask<String, Integer, String> {
        AlertDialog alertDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            builder = new AlertDialog.Builder(activity, R.style.Downloader_DialogTheme)
                    .setView(viewDialog)
                    .setCancelable(true);
            alertDialog = builder.create();
            alertDialog.show();
            rh_progress_bar.setMax(downloadDataArrayList.size());
        }

        @Override
        protected String doInBackground(String... sUrl) {
            for (int i = 0; i < downloadDataArrayList.size(); i++) {
                InputStream input = null;
                OutputStream output = null;
                HttpURLConnection connection = null;
                try {
                    java.net.URL url = new URL(downloadDataArrayList.get(i).getUrl());
                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();

                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        return "Server returned HTTP " + connection.getResponseCode()
                                + " " + connection.getResponseMessage();
                    }

                    int fileLength = connection.getContentLength();

                    input = connection.getInputStream();
                    File filename = new File(AppUtils.RootDirectoryInstaShow, downloadDataArrayList.get(i).getName());
                    output = new FileOutputStream(filename);
                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        total += count;
                        if (fileLength > 0)
                            publishProgress(i);
                        output.write(data, 0, count);
                    }
                    refreshGallery(filename.getPath());
                } catch (Exception e) {
                    return e.toString();
                } finally {
                    try {
                        if (output != null)
                            output.close();
                        if (input != null)
                            input.close();
                    } catch (IOException ignored) {
                    }

                    if (connection != null)
                        connection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.e("TAG", "onProgressUpdate: " + values[0]);
            rh_progress_bar.setProgress(values[0]);
            txtPerc.setText(values[0] + " %");
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            if (downloadDataArrayList.size() != 0) {
                downloadDataArrayList.clear();
                PhotoUrl = "";
                VideoUrl = "";
            }

            alertDialog.dismiss();
            Toast.makeText(activity, "Download Complete", Toast.LENGTH_SHORT).show();
        }
    }


    class DownloadSingleImage extends AsyncTask<String, Integer, String> {
        AlertDialog alertDialog;
        File filename;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            builder = new AlertDialog.Builder(activity, R.style.Downloader_DialogTheme)
                    .setView(viewDialog)
                    .setCancelable(true);
            alertDialog = builder.create();
            alertDialog.show();
            rh_progress_bar.setMax(100);
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                java.net.URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                int fileLength = connection.getContentLength();

                input = connection.getInputStream();
                filename = new File(AppUtils.RootDirectoryInstaShow, sUrl[1]);
                output = new FileOutputStream(filename);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    if (fileLength > 0)
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return filename.getPath();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            rh_progress_bar.setProgress(values[0]);
            txtPerc.setText(values[0] + " %");
        }

        @Override
        protected void onPostExecute(String path) {
            super.onPostExecute(path);
            alertDialog.dismiss();
            Toast.makeText(activity, "Download Complete", Toast.LENGTH_SHORT).show();
            refreshGallery(path);
            if (storiesListAdapter != null)
                storiesListAdapter.notifyDataSetChanged();
        }
    }

    private void refreshGallery(String path) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{new File(path).getAbsolutePath()}, new String[]{
                path.endsWith(".mp4") ? "video/*" : "image/*"}, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
            }

            public void onScanCompleted(String str, Uri uri) {
            }
        });
    }

    public void refreshDialogAd(FrameLayout adFrame) {
        if (mNativeAdDialog != null) {
            mNativeAdDialog.destroy();
        }
        new AdLoader.Builder(activity, PreferenceUtility.getInstance(getActivity()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) getLayoutInflater().inflate(R.layout.admob_native, null);
            mNativeAdDialog = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdDialog, unifiedNativeAdView);
            adFrame.removeAllViews();
            adFrame.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
//        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
//        if (unifiedNativeAd.getStarRating() == null) {
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
//        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }
}
