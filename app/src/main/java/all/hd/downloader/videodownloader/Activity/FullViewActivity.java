package all.hd.downloader.videodownloader.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;

import java.io.File;
import java.util.ArrayList;

import all.hd.downloader.videodownloader.Adapter.ShowImagesAdapter;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;

public class FullViewActivity extends AppCompatActivity {
    private int imgPosition = 0;
    private FullViewActivity activity;
    private ArrayList<File> fileArrayList;
    ViewPager vp_view;
    ShowImagesAdapter showImagesAdapter;
    MaterialButton imDelete, imShare;
    Toolbar toolbar;

    private UnifiedNativeAd mNativeAd;
    FrameLayout frameLayout;
    View view;
    AlertDialog.Builder builder;

    private com.google.android.gms.ads.InterstitialAd interstitialAd;
    private RelativeLayout banner_container;
    private com.google.android.gms.ads.AdView adm_banner;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_full_view);
        this.activity = this;
        init();

        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            if (PreferenceUtility.getInstance(FullViewActivity.this).getAdmobBanner().equals("yes"))
                loadBannerAdmob();

        if (PreferenceUtility.getInstance(FullViewActivity.this).getAdmobNative().equals("yes"))
            loadNativeAdsAdmob();

        if (getIntent().getExtras() != null) {
            this.fileArrayList = (ArrayList) getIntent().getSerializableExtra("ImageDataFile");
            this.imgPosition = getIntent().getIntExtra("Position", 0);
        }
        initAdapter();
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        vp_view = findViewById(R.id.vp_view);
        imDelete = findViewById(R.id.btnDelete);
        imShare = findViewById(R.id.btnShare);

        banner_container = findViewById(R.id.banner_container);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_banner());

        view = LayoutInflater.from(this).inflate(R.layout.delete_dialog, null);
        frameLayout = view.findViewById(R.id.adFrame);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    public void initAdapter() {
        showImagesAdapter = new ShowImagesAdapter(this, fileArrayList, this);
        vp_view.setAdapter(showImagesAdapter);
        vp_view.setCurrentItem(imgPosition);
        ((TextView) findViewById(R.id.app_title)).setText((vp_view.getCurrentItem() + 1) + " of " + fileArrayList.size());
        vp_view.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrollStateChanged(int i) {
            }

            @Override
            public void onPageScrolled(int i, float f, int i2) {
            }

            @Override
            public void onPageSelected(int i) {
                imgPosition = i;
                ((TextView) findViewById(R.id.app_title)).setText((vp_view.getCurrentItem() + 1) + " of " + fileArrayList.size());
            }
        });
        imDelete.setOnClickListener(view -> {
            deleteDialog();
        });
        imShare.setOnClickListener((View.OnClickListener) view -> {
            if (((File) FullViewActivity.this.fileArrayList.get(FullViewActivity.this.imgPosition)).getName().contains(".mp4")) {
                AppUtils.shareVideo(FullViewActivity.this.activity, ((File) FullViewActivity.this.fileArrayList.get(FullViewActivity.this.imgPosition)).getPath());
                return;
            }
            AppUtils.shareImage(FullViewActivity.this.activity, ((File) FullViewActivity.this.fileArrayList.get(FullViewActivity.this.imgPosition)).getPath());
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        this.activity = this;
        if (AppUtils.getInstance(FullViewActivity.this).isNetworkAvailable(FullViewActivity.this))
            if (PreferenceUtility.getInstance(FullViewActivity.this).getAdmobInterstitial().equals("yes"))
                loadInterstitialAdmob();
    }

    public void deleteFileAA(int i) {
        this.fileArrayList.remove(i);
        this.showImagesAdapter.notifyDataSetChanged();
        AppUtils.setToast(this.activity, getResources().getString(R.string.file_deleted));
        if (this.fileArrayList.size() == 0) {
            onBackPressed();
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == 16908332) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void deleteDialog() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        TextView ok = view.findViewById(R.id.btn_okay);
        ImageView cancel = view.findViewById(R.id.btn_cancel);

        if (mNativeAd != null && frameLayout != null) {
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }
        builder = new AlertDialog.Builder(this, R.style.Widget_Video_DialogTheme)
                .setView(view)
                .setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ok.setOnClickListener(arg0 -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            if (fileArrayList.get(imgPosition).delete()) {
                deleteFileAA(imgPosition);
            }
        });
        cancel.setOnClickListener(v -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
        });
        alertDialog.setOnDismissListener(dialog1 -> {
            if (frameLayout != null) {
                if (PreferenceUtility.getInstance(FullViewActivity.this).getAdmobNative().equals("yes"))
                    loadNativeAdsAdmob();
            }
            if ((view.findViewById(R.id.container_main)).getParent() != null) {
                ((ViewGroup) view.findViewById(R.id.container_main).getParent()).removeView(view.findViewById(R.id.container_main));
            }
        });
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        new AdLoader.Builder(this, PreferenceUtility.getInstance(getApplicationContext()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    @Override
    public void onBackPressed() {
        showInterstitialAdAdmob();
        finish();
    }

    @Override
    protected void onDestroy() {
        if (mNativeAd != null)
            mNativeAd.destroy();
        super.onDestroy();
    }

    public void loadInterstitialAdmob() {
        interstitialAd = new com.google.android.gms.ads.InterstitialAd(FullViewActivity.this);
        interstitialAd.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}