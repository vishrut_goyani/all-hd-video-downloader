package all.hd.downloader.videodownloader.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;

import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.Fragment.Download_Twitter_Fragment;
import all.hd.downloader.videodownloader.Fragment.TwitterFragment;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;
import nl.joery.animatedbottombar.AnimatedBottomBar;

public class Main_Twitter_Activity extends AppCompatActivity {
    ViewPager vp;
    Toolbar toolbar;
    AnimatedBottomBar bottomBar;

    private RelativeLayout banner_container;
    private com.google.android.gms.ads.AdView adm_banner;
    private com.google.android.gms.ads.InterstitialAd interstitialAd;

    UnifiedNativeAd mNativeAdSmall;
    FrameLayout adFrameSmall;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_twitter_activity);
        init();

        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            if (PreferenceUtility.getInstance(Main_Twitter_Activity.this).getAdmobBanner().equals("yes"))
                loadBannerAdmob();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (adm_banner != null)
            adm_banner.resume();

        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            if (PreferenceUtility.getInstance(Main_Twitter_Activity.this).getAdmobInterstitial().equals("yes"))
                loadInterstitialAdmob();

        if (PreferenceUtility.getInstance(Main_Twitter_Activity.this).getAdmobNative().equals("yes"))
            refreshAdSmall(adFrameSmall);
    }

    private void init() {
        vp = findViewById(R.id.vp);
        toolbar = findViewById(R.id.toolbar);
        bottomBar = findViewById(R.id.bottomBar);
        setSupportActionBar(toolbar);
        banner_container = findViewById(R.id.banner_container);

        adFrameSmall = findViewById(R.id.adFrameSmall);

        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_banner());

        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        View_Pager_Adapter view_pager_adapter = new View_Pager_Adapter(getSupportFragmentManager());
        vp.setAdapter(view_pager_adapter);
        bottomBar.setOnTabSelected(tab -> {
            switch (tab.getId()) {
                case R.id.bottom_images:
                    vp.setCurrentItem(0);
                    break;
                case R.id.bottom_video:
                    vp.setCurrentItem(1);
                    break;
            }
            return null;
        });
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomBar.selectTabAt(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    public void loadInterstitialAdmob() {
        interstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        interstitialAd.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class View_Pager_Adapter extends FragmentPagerAdapter {

        public View_Pager_Adapter(@NonNull FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new TwitterFragment();
                case 1:
                    return new Download_Twitter_Fragment();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public void refreshAdSmall(FrameLayout adFrameSmall) {
        if (mNativeAdSmall != null) {
            mNativeAdSmall.destroy();
        }
        new AdLoader.Builder(this, PreferenceUtility.getInstance(Main_Twitter_Activity.this).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) LayoutInflater.from(Main_Twitter_Activity.this).inflate(R.layout.admob_native_small, null);
            mNativeAdSmall = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdSmall, unifiedNativeAdView);
            adFrameSmall.removeAllViews();
            adFrameSmall.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                if (PreferenceUtility.getInstance(Main_Twitter_Activity.this).getAdmobInterstitial().equals("yes")) {
                    refreshAdSmallPersonal(adFrameSmall);
                }
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void refreshAdSmallPersonal(FrameLayout adFrameSmall) {
        if (mNativeAdSmall != null) {
            mNativeAdSmall.destroy();
        }
        new AdLoader.Builder(this, PreferenceUtility.getInstance(Main_Twitter_Activity.this).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) LayoutInflater.from(this).inflate(R.layout.admob_native_small, null);
            mNativeAdSmall = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdSmall, unifiedNativeAdView);
            adFrameSmall.removeAllViews();
            adFrameSmall.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getStarRating() == null) {
            unifiedNativeAdView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    @Override
    public void onBackPressed() {
        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            showInterstitialAdAdmob();
        finish();
    }

    @Override
    protected void onDestroy() {
        if (interstitialAd != null) {
            interstitialAd = null;
        }
        super.onDestroy();
    }
}
