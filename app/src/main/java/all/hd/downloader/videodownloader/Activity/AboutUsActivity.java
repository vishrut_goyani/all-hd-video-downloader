package all.hd.downloader.videodownloader.Activity;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import all.hd.downloader.videodownloader.BuildConfig;
import all.hd.downloader.videodownloader.R;

public class AboutUsActivity extends AppCompatActivity {

    TextView versionTxt;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        toolbar = findViewById(R.id.toolbar);
        versionTxt = findViewById(R.id.app_version);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        versionTxt.setText(getResources().getString(R.string.version_name, BuildConfig.VERSION_NAME));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}