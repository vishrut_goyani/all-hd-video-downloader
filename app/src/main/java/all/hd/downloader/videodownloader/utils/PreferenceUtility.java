package all.hd.downloader.videodownloader.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceUtility {
    public static SharedPreferences prefs;
    private static PreferenceUtility mInstance;
    public static Context mContext;
    public static final String APP_PREFS = "HD_DOWNLOADER _PREFS";
    public static final String speedEnabled = "speed_enabled";
    public static final String optimalEnabled = "optimal_enabled";
    public static final String darkModeStatus = "dark_mode_enabled";
    public static final String isConnected = "isConnected";
    public static final String keyPos = "keyPosition";
    public static final String admob_interstitial = "admob_interstitial";
    public static final String admob_banner = "admob_banner";
    public static final String admob_native = "admob_native";
    public static final String admob_open_app = "admob_open_app";

    public static String adm_app_id = "nothing";
    public static String adm_native = "nothing";
    public static String adm_banner = "nothing";
    public static String adm_interstitial = "nothing";
    public static String adm_open_app = "nothing";

    public PreferenceUtility(Context context) {
        mContext = context;
        try {
            prefs = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized PreferenceUtility getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceUtility(context);
        }
        return mInstance;
    }

    public void setAdmobInterstitial(String enabled) {
        prefs.edit().putString(admob_interstitial, enabled).apply();
    }

    public String getAdmobInterstitial() {
        return prefs.getString(admob_interstitial, "yes");
    }

    public void setAdmobBanner(String enabled) {
        prefs.edit().putString(admob_banner, enabled).apply();
    }

    public String getAdmobBanner() {
        return prefs.getString(admob_banner, "yes");
    }

    public void setAdmobNative(String enabled) {
        prefs.edit().putString(admob_native, enabled).apply();
    }

    public String getAdmobNative() {
        return prefs.getString(admob_native, "yes");
    }

    public void setAdmobOpenApp(String enabled) {
        prefs.edit().putString(admob_open_app, enabled).apply();
    }

    public String getAdmobOpenApp() {
        return prefs.getString(admob_open_app, "yes");
    }

    public static String getAdm_app_id() {
        return adm_app_id;
    }

    public static void setAdm_app_id(String adm_app_id) {
        PreferenceUtility.adm_app_id = adm_app_id;
    }

    public static String getAdm_banner() {
        return adm_banner;
    }

    public static void setAdm_banner(String adm_banner) {
        PreferenceUtility.adm_banner = adm_banner;
    }

    public static String getAdm_native() {
        return adm_native;
    }

    public static void setAdm_native(String adm_native) {
        PreferenceUtility.adm_native = adm_native;
    }

    public static String getAdm_interstitial() {
        return adm_interstitial;
    }

    public static void setAdm_interstitial(String adm_interstitial) {
        PreferenceUtility.adm_interstitial = adm_interstitial;
    }

    public static String getAdm_open_app() {
        return adm_open_app;
    }

    public static void setAdm_open_app(String adm_open_app) {
        PreferenceUtility.adm_open_app = adm_open_app;
    }
}
