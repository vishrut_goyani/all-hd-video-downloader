package all.hd.downloader.videodownloader.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import all.hd.downloader.videodownloader.Interfaces.UserListInterface;
import all.hd.downloader.videodownloader.models.story_list.TrayModel;
import all.hd.downloader.videodownloader.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<TrayModel> trayModelArrayList;
    private UserListInterface userListInterface;

    public UserListAdapter(Context context2, ArrayList<TrayModel> arrayList) {
        this.context = context2;
        this.trayModelArrayList = arrayList;
    }

    public void setClick(UserListInterface userListInterface2) {
        this.userListInterface = userListInterface2;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_user_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        if (trayModelArrayList.get(position).getUser() != null) {
            Log.e("TAG", "onBindViewHolder: " + trayModelArrayList.size());
            viewHolder.real_name.setText(trayModelArrayList.get(position).getUser().getFull_name());
            Glide.with(context).load(trayModelArrayList.get(position).getUser().getProfile_pic_url()).thumbnail(0.2f).into(viewHolder.story_icon);
            viewHolder.RLStoryLayout.setOnClickListener(view -> userListInterface.userListClick(position, trayModelArrayList.get(position)));
        }
    }

    @Override
    public int getItemCount() {
        if (trayModelArrayList == null) {
            return 0;
        }
        return trayModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView story_icon;
        TextView real_name, user_name;
        RelativeLayout RLStoryLayout;

        public ViewHolder(View view) {
            super(view);
            story_icon = view.findViewById(R.id.story_icon);
            real_name = view.findViewById(R.id.real_name);
            user_name = view.findViewById(R.id.user_name);
            RLStoryLayout = view.findViewById(R.id.RLStoryLayout);
        }
    }
}
