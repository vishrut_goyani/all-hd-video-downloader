package all.hd.downloader.videodownloader.models;

public class Device_Details {
    String device_manufacturer, device_model, device_token;
    int device_os;

    public Device_Details(String device_manufacturer, int device_os, String device_model, String device_token) {
        this.device_manufacturer = device_manufacturer;
        this.device_os = device_os;
        this.device_model = device_model;
        this.device_token = device_token;
    }

    public Device_Details() {
    }

    public String getDevice_manufacturer() {
        return device_manufacturer;
    }

    public void setDevice_manufacturer(String device_manufacturer) {
        this.device_manufacturer = device_manufacturer;
    }

    public int getDevice_os() {
        return device_os;
    }

    public void setDevice_os(int device_os) {
        this.device_os = device_os;
    }

    public String getDevice_model() {
        return device_model;
    }

    public void setDevice_model(String device_model) {
        this.device_model = device_model;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
