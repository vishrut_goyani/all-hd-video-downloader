package all.hd.downloader.videodownloader.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.constraintlayout.solver.widgets.analyzer.BasicMeasure;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.App_Main.ClipboardListener;
import all.hd.downloader.videodownloader.Interfaces.FeedbackListener;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.nativead.PersonalAds;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;
import all.hd.downloader.videodownloader.widgets.FeedbackSheetBuilder;
import dev.yuganshtyagi.smileyrating.SmileyRatingView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FeedbackListener {
    AppUpdateManager appUpdateManager;
    private static final int CHECK_UPDATE_REQUEST_CODE = 151;
    String CopyKey = "";
    String CopyValue = "";
    MainActivity activity;
    private ClipboardManager clipBoard;
    String[] permissions = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    LinearLayout rvInsta, rvWhatsApp, rvFB, rvTwitter, rvGallery, rvRateApp, rvShareApp;
    public static FeedbackListener feedbackListenerMain;
    FeedbackSheetBuilder feedbackSheetBuilder;
    SmileyRatingView smiley_view;
    AppCompatRatingBar ratingBar;
    TextView tv_submit, tv_maybe_later, rate_text;
    float rating;
    DrawerLayout drawerLayout;
    ImageView imgDrawer;
    boolean isShowing;

    BottomSheetDialog mDialog;
    private RelativeLayout banner_container;
    private UnifiedNativeAd mNativeAd, mNativeAdBack;
    private com.google.android.gms.ads.AdView adm_banner;
    FrameLayout frameLayout;
    boolean isLoadedBack;
    View view;
    FrameLayout adFrame;

    ImageView adsimg;
    TextView adsname;
    LinearLayout drawerads;
    String link;
    int count = 0;
    ArrayList<PersonalAds> adsList;
    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            setimages();
            handler.postDelayed(this, 2000);
        }
    };

    private void setimages() {
        if (adsList.size() != 0) {
            Glide.with(this).load(adsList.get(count).getAds_icon()).into(adsimg);
            adsname.setText(adsList.get(count).getAds_name());
            link = adsList.get(count).getAds_url();
            Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
            drawerads.startAnimation(shake);
            drawerads.setOnClickListener(view -> {
                try {
                    Uri uri = Uri.parse(link);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            count++;
            if (count == adsList.size()) {
                count = 0;
            }
        }
    }

    private void initPersonalAds() {
        adsList = new ArrayList<>();
        PersonalAds personalAds = new PersonalAds();
        personalAds.setAds_icon(R.drawable.free_vpn_icon);
        personalAds.setAds_name("Free VPN Hub");
        personalAds.setAds_url("https://play.google.com/store/apps/details?id=free.smart.vpn.vpnhubfree");
        adsList.add(personalAds);

        personalAds = new PersonalAds();
        personalAds.setAds_icon(R.drawable.best_photo_editor_icon);
        personalAds.setAds_name("Best Photo Editor");
        personalAds.setAds_url("https://play.google.com/store/apps/details?id=photoeditor.pro.picsart.collegemaker");
        adsList.add(personalAds);

        personalAds = new PersonalAds();
        personalAds.setAds_icon(R.drawable.sort_ball_icon);
        personalAds.setAds_name("Sort Ball Puzzle");
        personalAds.setAds_url("https://play.google.com/store/apps/details?id=com.sort.ball.puzzle");
        adsList.add(personalAds);

        handler.postDelayed(runnable, 1000);
    }

    private com.google.android.gms.ads.InterstitialAd mInterstitialAdmobVideo;

    public void loadInterStitialAdmobVideo() {
        mInterstitialAdmobVideo = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAdmobVideo.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
        mInterstitialAdmobVideo.loadAd(new AdRequest.Builder().build());
        mInterstitialAdmobVideo.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    private void showAdmobVideoAd() {
        if (mInterstitialAdmobVideo != null && mInterstitialAdmobVideo.isLoaded()) {
            mInterstitialAdmobVideo.show();
        }
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        this.activity = this;
        feedbackListenerMain = this;
        getDeviceToken();
        init();

        if (AppUtils.getScreenHeight(MainActivity.this) > 1280) {
            findViewById(R.id.app_title).setVisibility(View.VISIBLE);
            findViewById(R.id.imgDrawer).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.app_title).setVisibility(View.GONE);
            findViewById(R.id.imgDrawer).setVisibility(View.GONE);
        }

        view = LayoutInflater.from(this).inflate(R.layout.app_exit_dialog, null);
        adFrame = view.findViewById(R.id.adFrame);
        mDialog = new BottomSheetDialog(this, R.style.BottomSheetDialogTheme);
        mDialog.requestWindowFeature(1);
        mDialog.setContentView(view);

        if (AppUtils.getInstance(this).isNetworkAvailable(getApplicationContext())) {
            checkForAppUpdate();
        }
        ViewTreeObserver treeObserver = drawerLayout.getViewTreeObserver();
        treeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                drawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                AppUtils.updateGestureExclusionLeft(MainActivity.this);
            }
        });

        adsimg = findViewById(R.id.adsimg);
        adsname = findViewById(R.id.adsname);
        drawerads = findViewById(R.id.drawerads);
        initViews();
        initPersonalAds();
        AppUtils.getInstance(MainActivity.this).setCount(AppUtils.getInstance(MainActivity.this).getCounts() + 1);
    }

    public String getDeviceIMEI() {
        String secureId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        return secureId;
    }

    private void getDeviceToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    String token = task.getResult();
                    AppUtils.getInstance(this).setDeviceToken(token);
                });
    }

    private void init() {
        appUpdateManager = AppUpdateManagerFactory.create(this);
        rvInsta = findViewById(R.id.rvInsta);
        imgDrawer = findViewById(R.id.imgDrawer);
        rvWhatsApp = findViewById(R.id.rvWhatsApp);
        rvFB = findViewById(R.id.rvFB);
        rvTwitter = findViewById(R.id.rvTwitter);
        rvGallery = findViewById(R.id.rvGallery);
        rvShareApp = findViewById(R.id.rvShareApp);
        rvRateApp = findViewById(R.id.rvRateApp);
        drawerLayout = findViewById(R.id.drawer_layout);

        findViewById(R.id.nav_rateUs).setOnClickListener(this);
        findViewById(R.id.nav_feedback).setOnClickListener(this);
        findViewById(R.id.nav_shareApp).setOnClickListener(this);
        findViewById(R.id.nav_aboutUs).setOnClickListener(this);
        imgDrawer.setOnClickListener(v -> {
            drawerLayout.openDrawer(GravityCompat.START);
        });
        frameLayout = findViewById(R.id.frameLayout);
        banner_container = findViewById(R.id.banner_container);

        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_banner());
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private void checkForAppUpdate() {
        appUpdateManager.getAppUpdateInfo().addOnSuccessListener(result -> {
            if (result.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && result.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                try {
                    appUpdateManager.startUpdateFlowForResult(
                            result,
                            AppUpdateType.FLEXIBLE,
                            MainActivity.this,
                            CHECK_UPDATE_REQUEST_CODE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.activity = this;
        if (AppUtils.getInstance(this).isNetworkAvailable(getApplicationContext())) {
            checkForAppUpdate();
        }
        clipBoard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);

        if (AppUtils.getInstance(this).isNetworkAvailable(getApplicationContext())) {
            if (PreferenceUtility.getInstance(MainActivity.this).getAdmobNative().equals("yes"))
                refreshDialogAd(adFrame);

            if (PreferenceUtility.getInstance(MainActivity.this).getAdmobNative().equals("yes"))
                loadNativeAdsAdmob();

            if (PreferenceUtility.getInstance(MainActivity.this).getAdmobInterstitial().equals("yes"))
                loadInterStitialAdmobVideo();

            if (PreferenceUtility.getInstance(MainActivity.this).getAdmobBanner().equals("yes"))
                loadBannerAdmob();
        }
    }

    public void initViews() {
        this.clipBoard = (ClipboardManager) this.activity.getSystemService(CLIPBOARD_SERVICE);
        if (this.activity.getIntent().getExtras() != null) {
            for (String str : this.activity.getIntent().getExtras().keySet()) {
                this.CopyKey = str;
                String string = this.activity.getIntent().getExtras().getString(this.CopyKey);
                if (this.CopyKey.equals("android.intent.extra.TEXT")) {
                    this.CopyValue = this.activity.getIntent().getExtras().getString(this.CopyKey);
                    callText(string);
                } else {
                    this.CopyValue = "";
                    callText(string);
                }
            }
        }
        ClipboardManager clipboardManager = this.clipBoard;
        if (clipboardManager != null) {
            clipboardManager.addPrimaryClipChangedListener(new ClipboardListener() {
                public void onPrimaryClipChanged() {
                    try {
                        MainActivity mainActivity = MainActivity.this;
                        CharSequence text = mainActivity.clipBoard.getPrimaryClip().getItemAt(0).getText();
                        text.getClass();
                        mainActivity.showNotification(text.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        if (Build.VERSION.SDK_INT >= 23) {
            checkPermissions(0);
        }
        rvInsta.setOnClickListener(this);
        rvWhatsApp.setOnClickListener(this);
        rvFB.setOnClickListener(this);
        rvTwitter.setOnClickListener(this);
        rvGallery.setOnClickListener(this);
        rvShareApp.setOnClickListener(this);
        rvRateApp.setOnClickListener(this);
        AppUtils.createFileFolder();
    }

    private void callText(String str) {
        try {
            if (str.contains("instagram.com")) {
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(101);
                } else {
                    callInstaActivity();
                }
            } else if (str.contains("facebook.com")) {
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(104);
                } else {
                    callFacebookActivity();
                }
            } else if (!str.contains("twitter.com")) {
            } else {
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(106);
                } else {
                    callTwitterActivity();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rvFB:
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(104);
                    return;
                } else {
                    callFacebookActivity();
                    return;
                }

            case R.id.rvGallery:
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(105);
                    return;
                } else {
                    callGalleryActivity();
                    return;
                }
            case R.id.rvInsta:
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(101);
                    return;
                } else {
                    callInstaActivity();
                    return;
                }
            case R.id.rvRateApp:
                if (!AppUtils.getInstance(MainActivity.this).getRated())
                    showRatingDialog();
                else
                    Toast.makeText(activity, "You've rated us already!", Toast.LENGTH_SHORT).show();
                return;
            case R.id.rvShareApp:
                AppUtils.ShareApp(this.activity);
                return;
            case R.id.rvTwitter:
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(106);
                    return;
                } else {
                    callTwitterActivity();
                    return;
                }
            case R.id.rvWhatsApp:
                if (Build.VERSION.SDK_INT >= 23) {
                    checkPermissions(102);
                    return;
                } else {
                    callWhatsappActivity();
                    return;
                }
            case R.id.nav_rateUs:
                ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
                if (!AppUtils.getInstance(MainActivity.this).getRated()) {
                    isShowing = false;
                    showRatingDialog();
                } else {
                    Toast.makeText(activity, "You've rated us already!", Toast.LENGTH_SHORT).show();
                    AppUtils.getInstance(MainActivity.this).setRated(false);
                }
                break;
            case R.id.nav_shareApp:
                ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                String shareText = getResources().getString(R.string.share_app_desc, getPackageName());
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareText);
                startActivity(Intent.createChooser(shareIntent, "Share app"));
                break;
            case R.id.nav_feedback:
                ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
                onFeedbackGiven();
                break;
            case R.id.nav_aboutUs:
                startActivity(new Intent(this, AboutUsActivity.class));
                ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
                break;
            default:
                return;
        }
    }

    public void callInstaActivity() {
        Intent intent = new Intent(activity, Main_Instagram_Activity.class);
        intent.putExtra("CopyIntent", CopyValue);
        startActivity(intent);
        showAdmobVideoAd();
    }

    public void callWhatsappActivity() {
        startActivity(new Intent(MainActivity.this, Main_WhatsappActivity.class));
        showAdmobVideoAd();
    }

    public void callFacebookActivity() {
        Intent intent = new Intent(activity, Main_Facebook_Activity.class);
        intent.putExtra("CopyIntent", CopyValue);
        startActivity(intent);
        showAdmobVideoAd();
    }

    public void callTwitterActivity() {
        Intent intent = new Intent(activity, Main_Twitter_Activity.class);
        intent.putExtra("CopyIntent", CopyValue);
        startActivity(intent);
        showAdmobVideoAd();
    }

    public void callGalleryActivity() {
        startActivity(new Intent(this.activity, ALLCreationGalleryActivity.class));
        try {
            showAdmobVideoAd();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("WrongConstant")
    public void showNotification(String str) {
        if (str.contains("instagram.com") || str.contains("facebook.com") || str.contains("tiktok.com")) {
            Intent intent = new Intent(this.activity, MainActivity.class);
            intent.addFlags(67108864);
            intent.putExtra("Notification", str);
            @SuppressLint("WrongConstant") PendingIntent activity2 = PendingIntent.getActivity(this.activity, 0, intent, BasicMeasure.EXACTLY);
            NotificationManager notificationManager = (NotificationManager) this.activity.getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = new NotificationChannel(getResources().getString(R.string.app_name), getResources().getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.enableLights(true);
                notificationChannel.setLockscreenVisibility(1);
                notificationManager.createNotificationChannel(notificationChannel);
            }
            notificationManager.notify(1, new NotificationCompat.Builder(this.activity, getResources().getString(R.string.app_name)).setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher_round).setColor(getResources().getColor(R.color.black)).setLargeIcon(BitmapFactory.decodeResource(this.activity.getResources(), R.mipmap.ic_launcher_round)).setDefaults(-1).setPriority(1).setContentTitle("Copied text").setContentText(str).setChannelId(getResources().getString(R.string.app_name)).setFullScreenIntent(activity2, true).build());
        }
    }

    private boolean checkPermissions(int i) {
        ArrayList arrayList = new ArrayList();
        String[] strArr = this.permissions;
        for (String str : strArr) {
            if (ContextCompat.checkSelfPermission(this.activity, str) != 0) {
                arrayList.add(str);
            }
        }
        if (!arrayList.isEmpty()) {
            ActivityCompat.requestPermissions(this.activity, (String[]) arrayList.toArray(new String[arrayList.size()]), i);
            return false;
        } else if (i == 101) {
            callInstaActivity();
            return true;
        } else if (i == 102) {
            callWhatsappActivity();
            return true;
        } else if (i == 104) {
            callFacebookActivity();
            return true;
        } else if (i == 105) {
            callGalleryActivity();
            return true;
        } else if (i != 106) {
            return true;
        } else {
            callTwitterActivity();
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == 101) {
            if (iArr.length > 0 && iArr[0] == 0) {
                callInstaActivity();
            }
        } else if (i == 102) {
            if (iArr.length > 0 && iArr[0] == 0) {
                callWhatsappActivity();
            }
        } else if (i == 104) {
            if (iArr.length > 0 && iArr[0] == 0) {
                callFacebookActivity();
            }
        } else if (i == 105) {
            if (iArr.length > 0 && iArr[0] == 0) {
                callGalleryActivity();
            }
        } else if (i == 106 && iArr.length > 0 && iArr[0] == 0) {
            callTwitterActivity();
        }
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBackPressed() {
        showExitSheet();
    }

    private void showExitSheet() {
        mDialog.findViewById(R.id.btn_exit).setOnClickListener(view -> {
            mDialog.dismiss();
            finishAffinity();
            System.exit(0);
        });
        mDialog.setOnDismissListener(dialog -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
                mNativeAd = null;
            }
            if (PreferenceUtility.getInstance(MainActivity.this).getAdmobNative().equals("yes"))
                refreshDialogAd(adFrame);
        });
        mDialog.show();
    }

    public void refreshDialogAd(FrameLayout adFrame) {
        if (isLoadedBack && mNativeAdBack != null) {
            mNativeAdBack.destroy();
        }
        new AdLoader.Builder(this, PreferenceUtility.getInstance(getApplicationContext()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) getLayoutInflater().inflate(R.layout.admob_native, null);
            mNativeAdBack = unifiedNativeAd;
            isLoadedBack = true;
            populateUnifiedNativeAdViewBack(mNativeAdBack, unifiedNativeAdView);
            adFrame.removeAllViews();
            adFrame.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdViewBack(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
//        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
//        if (unifiedNativeAd.getStarRating() == null) {
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
//        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    @Override
    public void onFeedbackGiven() {
        feedbackSheetBuilder = FeedbackSheetBuilder.with(getSupportFragmentManager())
                .title("Feedback Form")
                .setupLayout(MainActivity.this, R.layout.bottom_sheet_feedback);
        View feedbackView = feedbackSheetBuilder.getLayout();
        initFeedbackheet(feedbackView);
        feedbackSheetBuilder.show();
    }

    private void initFeedbackheet(View feedbackView) {
        EditText editMail = feedbackView.findViewById(R.id.editMail);
        EditText editDesc = feedbackView.findViewById(R.id.editFeedback);
        Button sendEmail = feedbackView.findViewById(R.id.SendEmail);
        ImageView ic_close = feedbackView.findViewById(R.id.ic_close);

        sendEmail.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{AppUtils.RECEIVER_ADDRESS});
            intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
            intent.putExtra(Intent.EXTRA_TEXT,
                    "Email" + " : " + editMail.getText().toString() + "\n"
                            + "Description" + " : " + editDesc.getText().toString());
            try {
                AppUtils.getInstance(this).setRated(true);
                startActivity(Intent.createChooser(intent, "Send Mail"));
            } catch (ActivityNotFoundException ex) {
                ex.printStackTrace();
                Toast.makeText(MainActivity.this, getResources().getString(R.string.no_mail_found), Toast.LENGTH_SHORT).show();
            }
        });
        ic_close.setOnClickListener(v -> feedbackSheetBuilder.dismiss());
    }

    private void showRatingDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.best_rating_dialog, null);
        initDialog(view);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setView(view);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        rate_text.setText(getResources().getString(R.string.rating_desc));

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            smiley_view.setSmiley(rating <= 4.5 ? rating : rating - 1);
            this.rating = rating;
        });

        tv_submit.setOnClickListener(v -> {
            if (rating >= 4) {
                AppUtils.getInstance(MainActivity.this).setRated(true);
                alertDialog.dismiss();
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
            } else if (rating > 0) {
                alertDialog.dismiss();
                feedbackListenerMain.onFeedbackGiven();
            } else {
                Toast.makeText(MainActivity.this, "Ratings can't be empty!", Toast.LENGTH_SHORT).show();
            }
        });

        tv_maybe_later.setOnClickListener(v -> {
            if (isShowing) {
                AppUtils.getInstance(MainActivity.this).setCount(0);
                alertDialog.dismiss();
                finish();
            } else {
                alertDialog.dismiss();
            }
        });
    }

    private void initDialog(View view) {
        rate_text = view.findViewById(R.id.rate_text);
        smiley_view = view.findViewById(R.id.smiley_view);
        ratingBar = view.findViewById(R.id.ratingBar);
        tv_submit = view.findViewById(R.id.tv_submit);
        tv_maybe_later = view.findViewById(R.id.tv_later);
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        new AdLoader.Builder(this, PreferenceUtility.getInstance(getApplicationContext()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            if (MainActivity.this.isDestroyed()) {
                mNativeAd.destroy();
                return;
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHECK_UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.Widget_Alert_DialogTheme)
                        .setCancelable(false)
                        .setTitle("Try again!")
                        .setMessage("Update failed\nDo you want to try again?")
                        .setPositiveButton("Yes", (dialog, which) -> {
                            checkForAppUpdate();
                            dialog.dismiss();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> {
                            dialog.dismiss();
                            Snackbar updateCancelBar = Snackbar.make(findViewById(R.id.drawer_layout), "You can still update this app from Playstore.", 3000);
                            updateCancelBar.setAction("Ok", v -> {
                                updateCancelBar.dismiss();
                            });
                            updateCancelBar.show();
                        })
                        .setNeutralButton("Later", (dialog, which) -> {
                            dialog.dismiss();
                            Snackbar updateLaterBar = Snackbar.make(findViewById(R.id.drawer_layout), "As you wish!", 3000);
                            updateLaterBar.setAction(":)", v -> {
                                updateLaterBar.dismiss();
                            });
                            updateLaterBar.show();
                        })
                        .create();
                alertDialog.show();
            } else {
                Snackbar updateSuccessBar = Snackbar.make(findViewById(R.id.drawer_layout), "App updated successfully!", 3000);
                updateSuccessBar.setAction("\uD83D\uDC4D", v -> {
                    updateSuccessBar.dismiss();
                });
                updateSuccessBar.show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (adm_banner != null) {
            adm_banner.destroy();
        }
        if (mNativeAd != null) {
            mNativeAd.destroy();
        }
        handler.removeCallbacks(runnable);
        super.onDestroy();
    }
}
