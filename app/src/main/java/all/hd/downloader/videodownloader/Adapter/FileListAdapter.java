package all.hd.downloader.videodownloader.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import all.hd.downloader.videodownloader.Interfaces.FileListClickInterface;

import all.hd.downloader.videodownloader.R;

import java.io.File;
import java.util.ArrayList;

public class FileListAdapter extends RecyclerView.Adapter<FileListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<File> fileArrayList;
    private FileListClickInterface fileListClickInterface;

    public FileListAdapter(Context context2, ArrayList<File> arrayList, FileListClickInterface fileListClickInterface2) {
        this.context = context2;
        this.fileArrayList = arrayList;
        this.fileListClickInterface = fileListClickInterface2;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.items_file_view, viewGroup, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        final File file = this.fileArrayList.get(position);
        try {
            if (file.getName().substring(file.getName().lastIndexOf(".")).equals(".mp4")) {
                viewHolder.iv_play.setVisibility(View.VISIBLE);
            } else {
                viewHolder.iv_play.setVisibility(View.GONE);
            }
            Glide.with(this.context).load(file.getPath()).into(viewHolder.pc);
        } catch (Exception unused) {
        }
        viewHolder.rl_main.setOnClickListener(view -> FileListAdapter.this.fileListClickInterface.getPosition(position, file));
        viewHolder.popup.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(context, v);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.share:
                        Intent intentShare = new Intent("android.intent.action.SEND");
                        intentShare.setType("image/*");
                        intentShare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
                        intentShare.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(Intent.createChooser(intentShare, "Share"));
                        return true;
                    case R.id.delete:
                        File fdelete = new File(String.valueOf(file));
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                fileArrayList.remove(position);
                                notifyDataSetChanged();
                                Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Delete Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                        return true;
                    default:
                        return true;
                }
            });
            popupMenu.getMenu().add(1, R.id.share, 1, "Share");
            popupMenu.getMenu().add(1, R.id.delete, 2, "Delete");
            popupMenu.show();
        });
    }

    @Override
    public int getItemCount() {
        ArrayList<File> arrayList = this.fileArrayList;
        if (arrayList == null) {
            return 0;
        }
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_main;
        ImageView pc, iv_play, popup;
        TextView tv_fileName;

        public ViewHolder(View itemsFileViewBinding) {
            super(itemsFileViewBinding);
            tv_fileName = itemsFileViewBinding.findViewById(R.id.tv_fileName);
            popup = itemsFileViewBinding.findViewById(R.id.popup);
            rl_main = itemsFileViewBinding.findViewById(R.id.rl_main);
            pc = itemsFileViewBinding.findViewById(R.id.pc);
            iv_play = itemsFileViewBinding.findViewById(R.id.iv_play);
        }
    }
}
