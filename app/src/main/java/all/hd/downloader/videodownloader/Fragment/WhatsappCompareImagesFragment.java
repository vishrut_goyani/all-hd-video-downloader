package all.hd.downloader.videodownloader.Fragment;

import java.util.Comparator;

public final class WhatsappCompareImagesFragment implements Comparator {
    public static final WhatsappCompareImagesFragment INSTANCE = new WhatsappCompareImagesFragment();

    private WhatsappCompareImagesFragment() {
    }

    @Override
    public final int compare(Object obj, Object obj2) {
        return WhatsappImageFragment.getData(obj, obj2);
    }
}
