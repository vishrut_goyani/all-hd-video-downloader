package all.hd.downloader.videodownloader.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import all.hd.downloader.videodownloader.Activity.FullViewActivity;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.Interfaces.FileListClickInterface;

import all.hd.downloader.videodownloader.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class Download_Facebook_Fragment extends Fragment implements FileListClickInterface {
    RecyclerView download_data;
    ArrayList<File> files = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.download_fragment_twitter, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        download_data = view.findViewById(R.id.download_data);
    }

    @Override
    public void onResume() {
        super.onResume();
        getImages();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getImages();
        }
    }

    public void getImages() {
        if (AppUtils.RootDirectoryFacebookShow.exists()) {
            if (files.size() != 0) {
                files.clear();
            }
            File[] listFiles = AppUtils.RootDirectoryFacebookShow.listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    files.add(file);
                }
                GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
                download_data.setLayoutManager(gridLayoutManager);
                Collections.reverse(files);
                DownloadData downloadData = new DownloadData(getActivity(), files, this);
                download_data.setAdapter(downloadData);
            }
        }
    }

    @Override
    public void getPosition(int i, File file) {
        Intent intent = new Intent(getActivity(), FullViewActivity.class);
        intent.putExtra("ImageDataFile", files);
        intent.putExtra("Position", i);
        startActivity(intent);
    }

    public class DownloadData extends RecyclerView.Adapter<DownloadData.myh> {
        Context context;
        ArrayList<File> files;
        private FileListClickInterface fileListClickInterface;

        public DownloadData(Context context, ArrayList<File> files, FileListClickInterface fileListClickInterface) {
            this.context = context;
            this.files = files;
            this.fileListClickInterface = fileListClickInterface;
        }

        @NonNull
        @Override
        public myh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.download_item, parent, false);
            return new myh(view);
        }

        @Override
        public void onBindViewHolder(@NonNull DownloadData.myh holder, int position) {
            Glide.with(context).load(files.get(position)).into(holder.img_show);
            String path = String.valueOf(files.get(position));
            if (path.contains(".mp4")) {
                holder.play.setVisibility(View.VISIBLE);
            } else {
                holder.play.setVisibility(View.GONE);
            }

            holder.popup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupMenu popupMenu = new PopupMenu(context, v);
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()) {
                                case R.id.share:
                                    Intent intentShare = new Intent("android.intent.action.SEND");
                                    intentShare.setType("image/*");
                                    intentShare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", files.get(position));
                                    intentShare.putExtra(Intent.EXTRA_STREAM, uri);
                                    startActivity(Intent.createChooser(intentShare, "Share"));
                                    return true;
                                case R.id.delete:
                                    File fdelete = new File(String.valueOf(files.get(position)));
                                    if (fdelete.exists()) {
                                        if (fdelete.delete()) {
                                            files.remove(position);
                                            notifyDataSetChanged();
                                            Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(context, "Delete Failed", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    return true;
                                default:
                                    return true;
                            }
                        }
                    });
                    popupMenu.getMenu().add(1, R.id.share, 1, "Share");
                    popupMenu.getMenu().add(1, R.id.delete, 2, "Delete");
                    popupMenu.show();
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fileListClickInterface.getPosition(position, files.get(position));
                }
            });
        }

        @Override
        public int getItemCount() {
            return files.size();
        }

        public class myh extends RecyclerView.ViewHolder {
            ImageView img_show, play, popup;

            public myh(@NonNull View itemView) {
                super(itemView);
                img_show = itemView.findViewById(R.id.img_show);
                play = itemView.findViewById(R.id.play);
                popup = itemView.findViewById(R.id.popup);
            }
        }
    }
}
