package all.hd.downloader.videodownloader.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import all.hd.downloader.videodownloader.App_Main.SharePrefs;

import all.hd.downloader.videodownloader.R;

public class InstaLoginActivity extends AppCompatActivity {
    InstaLoginActivity activity;
    private String cookies;
    SwipeRefreshLayout swipeRefreshLayout;
    WebView webView;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_login);
        this.activity = this;
        init();
        LoadPage();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                InstaLoginActivity.this.LoadPage();
            }
        });
    }

    private void init() {
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        webView = findViewById(R.id.webView);
    }

    public void LoadPage() {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.setWebViewClient(new MyBrowser());
        CookieSyncManager.createInstance(this.activity);
        CookieManager.getInstance().removeAllCookie();
        webView.loadUrl("https://www.instagram.com/accounts/login/");
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView webView, int i) {
                if (i == 100) {
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    swipeRefreshLayout.setRefreshing(true);
                }
            }
        });
    }

    public String getCookie(String str, String str2) {
        String cookie = CookieManager.getInstance().getCookie(str);
        if (cookie != null && !cookie.isEmpty()) {
            String[] split = cookie.split(";");
            for (String str3 : split) {
                if (str3.contains(str2)) {
                    return str3.split("=")[1];
                }
            }
        }
        return null;
    }

    public class MyBrowser extends WebViewClient {
        private MyBrowser() {
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            webView.loadUrl(str);
            return true;
        }

        public void onLoadResource(WebView webView, String str) {
            super.onLoadResource(webView, str);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            InstaLoginActivity.this.cookies = CookieManager.getInstance().getCookie(str);
            try {
                String cookie = InstaLoginActivity.this.getCookie(str, "sessionid");
                String cookie2 = InstaLoginActivity.this.getCookie(str, "csrftoken");
                String cookie3 = InstaLoginActivity.this.getCookie(str, "ds_user_id");
                if (cookie != null && cookie2 != null && cookie3 != null) {
                    SharePrefs.getInstance(InstaLoginActivity.this.activity).putString(SharePrefs.COOKIES, InstaLoginActivity.this.cookies);
                    SharePrefs.getInstance(InstaLoginActivity.this.activity).putString(SharePrefs.CSRF, cookie2);
                    SharePrefs.getInstance(InstaLoginActivity.this.activity).putString(SharePrefs.SESSIONID, cookie);
                    SharePrefs.getInstance(InstaLoginActivity.this.activity).putString(SharePrefs.USERID, cookie3);
                    SharePrefs.getInstance(InstaLoginActivity.this.activity).putBoolean(SharePrefs.ISINSTALOGIN, true);
                    webView.destroy();
                    Intent intent = new Intent();
                    intent.putExtra("result", "result");
                    InstaLoginActivity.this.setResult(-1, intent);
                    InstaLoginActivity.this.finish();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
            return super.shouldInterceptRequest(webView, webResourceRequest);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return super.shouldOverrideUrlLoading(webView, webResourceRequest);
        }
    }
}
