package all.hd.downloader.videodownloader.Activity;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import all.hd.downloader.videodownloader.R;

public class View_Image_Activity extends AppCompatActivity {
    ImageView img_show, back;
    String img_path;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_image_activity);
        init();
    }

    private void init() {
        img_path = getIntent().getStringExtra("path");
        img_show = findViewById(R.id.img_show);
        back = findViewById(R.id.back);
        Glide.with(View_Image_Activity.this).load(img_path).into(img_show);
        back.setOnClickListener(v -> onBackPressed());
    }
}
