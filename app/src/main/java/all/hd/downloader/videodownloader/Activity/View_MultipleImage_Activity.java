package all.hd.downloader.videodownloader.Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.Fragment.WhatsappImageFragment;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.models.WhatsappStatusModel;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;

public class View_MultipleImage_Activity extends AppCompatActivity {
    ViewPager vp;
    int pos, Position;
    ExtendedFloatingActionButton btnDownload;
    Toolbar toolbar;

    private RelativeLayout banner_container;
    private com.google.android.gms.ads.AdView adm_banner;

    LinearLayout nativeAdView;
    private UnifiedNativeAd mNativeAd;
    FrameLayout rl_fb_ad;
    FrameLayout frameLayout;
    View view;
    AlertDialog.Builder builder;

    private com.google.android.gms.ads.InterstitialAd interstitialAd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_multipleimage_activity);

        init();
        if (AppUtils.getInstance(View_MultipleImage_Activity.this).isNetworkAvailable(View_MultipleImage_Activity.this))
            if (PreferenceUtility.getInstance(View_MultipleImage_Activity.this).getAdmobInterstitial().equals("yes"))
                loadInterstitialAdmob();

        if (PreferenceUtility.getInstance(View_MultipleImage_Activity.this).getAdmobNative().equals("yes"))
            loadNativeAdsAdmob();
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        btnDownload = findViewById(R.id.btnDownload);
        pos = getIntent().getIntExtra("pos", 0);
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Position = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if (getIntent().getBooleanExtra("isDownloaded", false)) {
            btnDownload.setText("Delete");
            btnDownload.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_delete, 0);
        } else {
            btnDownload.setText("Download");
            btnDownload.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_download_24, 0);
        }

        btnDownload.setOnClickListener(v -> {
            if (Objects.equals(btnDownload.getText().toString().toLowerCase(), "delete")) {
                deleteDialog();
            } else {
                AppUtils.createFileFolder();
                String path = WhatsappImageFragment.statusModelArrayList.get(Position).getPath();
                String substring = path.substring(path.lastIndexOf("/") + 1);
                try {
                    FileUtils.copyFileToDirectory(new File(path), new File(AppUtils.RootDirectoryWhatsappShow + "/"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String substring2 = substring.substring(12);
                MediaScannerConnection.scanFile(View_MultipleImage_Activity.this, new String[]{new File(AppUtils.RootDirectoryWhatsappShow + "/" + substring2).getAbsolutePath()}, new String[]{WhatsappImageFragment.statusModelArrayList.get(Position).getUri().toString().endsWith(".mp4") ? "video/*" : "image/*"}, new MediaScannerConnection.MediaScannerConnectionClient() {
                    public void onMediaScannerConnected() {
                    }

                    public void onScanCompleted(String str, Uri uri) {
                    }
                });
                btnDownload.setText("Delete");
                btnDownload.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_delete, 0);
                Toast.makeText(View_MultipleImage_Activity.this, getResources().getString(R.string.saved_to) + AppUtils.RootDirectoryWhatsappShow + "/" + substring2, Toast.LENGTH_LONG).show();
            }
        });

        if (WhatsappImageFragment.statusModelArrayList.size() != 0) {
            ShowImagesAdapter showImagesAdapter = new ShowImagesAdapter(View_MultipleImage_Activity.this, WhatsappImageFragment.statusModelArrayList);
            vp.setAdapter(showImagesAdapter);
            vp.setCurrentItem(pos);
        }
    }

    private void init() {
        vp = findViewById(R.id.vp);
        toolbar = findViewById(R.id.toolbar);

        view = LayoutInflater.from(this).inflate(R.layout.delete_dialog, null);
        rl_fb_ad = view.findViewById(R.id.rl_fb_ad);
        frameLayout = view.findViewById(R.id.adFrame);

        banner_container = findViewById(R.id.banner_container);
        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_banner());
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceUtility.getInstance(View_MultipleImage_Activity.this).getAdmobBanner().equals("yes"))
            loadBannerAdmob();
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    public class ShowImagesAdapter extends PagerAdapter {
        Context context;
        ArrayList<WhatsappStatusModel> imageList;
        LayoutInflater inflater;

        public ShowImagesAdapter(Context context2, ArrayList<WhatsappStatusModel> arrayList) {
            this.context = context2;
            this.imageList = arrayList;
            this.inflater = LayoutInflater.from(context2);
        }

        @Override
        public void destroyItem(ViewGroup viewGroup, int i, @NotNull Object obj) {
            viewGroup.removeView((View) obj);
        }

        @NotNull
        @SuppressLint("WrongConstant")
        @Override
        public Object instantiateItem(@NotNull ViewGroup viewGroup, final int i) {
            View inflate = inflater.inflate(R.layout.view_image_item, viewGroup, false);
            ImageView img_view = inflate.findViewById(R.id.img_view);
            Glide.with(context).load(imageList.get(i).getPath()).into(img_view);
            viewGroup.addView(inflate, 0);
            return inflate;
        }

        @Override
        public int getCount() {
            return imageList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view.equals(obj);
        }
    }

    public void loadNativeAdsAdmob() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        new AdLoader.Builder(this, PreferenceUtility.getInstance(getApplicationContext()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            if (mNativeAd != null) {
                mNativeAd.destroy();
            }
            mNativeAd = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.setVisibility(View.VISIBLE);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                frameLayout.setVisibility(View.GONE);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());

    }


    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.txt_callToAction));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }

    public void deleteDialog() {
        UnifiedNativeAdView adView = (UnifiedNativeAdView) getLayoutInflater()
                .inflate(R.layout.admob_native_home, null);
        TextView ok = view.findViewById(R.id.btn_okay);
        ImageView cancel = view.findViewById(R.id.btn_cancel);

        if (mNativeAd != null && frameLayout != null) {
            populateUnifiedNativeAdView(mNativeAd, adView);
            frameLayout.removeAllViews();
            frameLayout.addView(adView);
        }
        builder = new AlertDialog.Builder(this, R.style.Widget_Video_DialogTheme)
                .setView(view)
                .setCancelable(true);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

        ok.setOnClickListener(arg0 -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            String path = WhatsappImageFragment.statusModelArrayList.get(Position).getPath();
            String substring = path.substring(path.lastIndexOf("/") + 1);
            String substring2 = substring.substring(12);
            String path1 = AppUtils.RootDirectoryWhatsappShow + "/" + substring2;
            File fdelete = new File(path1);
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
                }
            }
            btnDownload.setText("Download");
            btnDownload.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_download_24, 0);
        });
        cancel.setOnClickListener(v -> {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
        });
        alertDialog.setOnDismissListener(dialog1 -> {
            if (frameLayout != null) {
                if (PreferenceUtility.getInstance(View_MultipleImage_Activity.this).getAdmobNative().equals("yes"))
                    loadNativeAdsAdmob();
            }
            if ((view.findViewById(R.id.container_main)).getParent() != null) {
                ((ViewGroup) view.findViewById(R.id.container_main).getParent()).removeView(view.findViewById(R.id.container_main));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNativeAd != null)
            mNativeAd.destroy();
    }

    public void loadInterstitialAdmob() {
        interstitialAd = new com.google.android.gms.ads.InterstitialAd(View_MultipleImage_Activity.this);
        interstitialAd.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        showInterstitialAdAdmob();
        finish();
    }
}
