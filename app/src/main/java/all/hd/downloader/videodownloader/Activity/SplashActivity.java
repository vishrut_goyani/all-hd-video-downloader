package all.hd.downloader.videodownloader.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 5000;
    private static int FIREBASE_TIME_OUT = 1000;

    private com.google.android.gms.ads.InterstitialAd mInterstitialAd;


    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_splash_screen);
        FirebaseAnalytics.getInstance(this);
        FirebaseCrashlytics.getInstance();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PreferenceUtility.getInstance(SplashActivity.this).getAdmobInterstitial().equals("yes"))
                    loadInterstitialAdAdmob();
            }
        }, FIREBASE_TIME_OUT);

        new Handler()
                .postDelayed(() -> {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    showInterstitialAdAdmob();
                    finish();
                }, SPLASH_TIME_OUT);
    }

    public void loadInterstitialAdAdmob() {
        mInterstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        mInterstitialAd.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
