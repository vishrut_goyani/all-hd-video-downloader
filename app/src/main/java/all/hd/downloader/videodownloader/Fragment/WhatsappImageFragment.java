package all.hd.downloader.videodownloader.Fragment;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import all.hd.downloader.videodownloader.Adapter.WhatsappStatusAdapter;
import all.hd.downloader.videodownloader.models.WhatsappStatusModel;
import all.hd.downloader.videodownloader.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class WhatsappImageFragment extends Fragment {
    private File[] allfiles;
    public static ArrayList<WhatsappStatusModel> statusModelArrayList;
    private WhatsappStatusAdapter whatsappStatusAdapter;
    SwipeRefreshLayout swiperefresh;
    TextView tvNoResult;
    RecyclerView rvFileList;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.fragment_whatsapp_image, viewGroup, false);
        init(view);
        initViews();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshWhatsappImageFragment();
    }

    private void init(View view) {
        swiperefresh = view.findViewById(R.id.swiperefresh);
        tvNoResult = view.findViewById(R.id.tv_NoResult);
        rvFileList = view.findViewById(R.id.rv_fileList);
    }

    private void initViews() {
        statusModelArrayList = new ArrayList<>();
        getData();
        swiperefresh.setOnRefreshListener(() -> refreshWhatsappImageFragment());
    }

    public void refreshWhatsappImageFragment() {
        statusModelArrayList = new ArrayList<>();
        getData();
        swiperefresh.setRefreshing(false);
    }

    private void getData() {
        this.allfiles = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp/Media/.Statuses").listFiles();
        File[] listFiles = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/WhatsApp Business/Media/.Statuses").listFiles();
        try {
            Arrays.sort(this.allfiles, WhatsappCompareImagesFragment.INSTANCE);
            for (int j = 0; j < allfiles.length; j++) {
                File[] fileArr = this.allfiles;
                File file = fileArr[j];
                if (Uri.fromFile(file).toString().endsWith(".png") || Uri.fromFile(file).toString().endsWith(".jpg")) {
                    this.statusModelArrayList.add(new WhatsappStatusModel("WhatsStatus: " + (j + 1), Uri.fromFile(file), this.allfiles[j].getAbsolutePath(), file.getName()));
                }
            }

        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (this.statusModelArrayList.size() != 0) {
            tvNoResult.setVisibility(View.GONE);
        } else {
            tvNoResult.setVisibility(View.VISIBLE);
        }
        this.whatsappStatusAdapter = new WhatsappStatusAdapter(getActivity(), this.statusModelArrayList);
        rvFileList.setNestedScrollingEnabled(false);
        rvFileList.setAdapter(this.whatsappStatusAdapter);
    }

    static int getData(Object obj, Object obj2) {
        File file = (File) obj;
        File file2 = (File) obj2;
        if (file.lastModified() > file2.lastModified()) {
            return -1;
        }
        return file.lastModified() < file2.lastModified() ? 1 : 0;
    }

    static int getData2(Object obj, Object obj2) {
        File file = (File) obj;
        File file2 = (File) obj2;
        if (file.lastModified() > file2.lastModified()) {
            return -1;
        }
        return file.lastModified() < file2.lastModified() ? 1 : 0;
    }
}
