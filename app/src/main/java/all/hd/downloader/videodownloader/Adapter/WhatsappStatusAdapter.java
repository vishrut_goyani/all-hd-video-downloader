package all.hd.downloader.videodownloader.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import all.hd.downloader.videodownloader.Activity.View_MultipleImage_Activity;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.models.WhatsappStatusModel;
import all.hd.downloader.videodownloader.utils.AppConstants;

public class WhatsappStatusAdapter extends RecyclerView.Adapter<WhatsappStatusAdapter.ViewHolder> {
    public String SaveFilePath = (AppUtils.RootDirectoryWhatsappShow + "/");
    private Context context;
    public ArrayList<WhatsappStatusModel> fileArrayList;

    public WhatsappStatusAdapter(Context context2, ArrayList<WhatsappStatusModel> arrayList) {
        this.context = context2;
        this.fileArrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.items_whatsapp_view, viewGroup, false);
        return new ViewHolder(view);
    }

    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final WhatsappStatusModel whatsappStatusModel = this.fileArrayList.get(position);
        if (whatsappStatusModel.getUri().toString().endsWith(".mp4")) {
            viewHolder.iv_play.setVisibility(View.VISIBLE);
        } else {
            viewHolder.iv_play.setVisibility(View.GONE);
        }
        Glide.with(this.context).load(whatsappStatusModel.getPath()).into(viewHolder.pcw);

        File file = new File(AppUtils.RootDirectoryWhatsappShow + "/" + whatsappStatusModel.getFilename());
        if (file.exists()) {
            viewHolder.tv_download.setText("Downloaded");
            viewHolder.popup.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tv_download.setText("Download");
            viewHolder.popup.setVisibility(View.GONE);
        }

        viewHolder.itemView.setOnClickListener(v -> {
            if (whatsappStatusModel.getUri().toString().endsWith(".mp4")) {
//                Intent intent = new Intent(context, Video_View_Activity.class);
//                intent.putExtra("path", whatsappStatusModel.getPath());
//                context.startActivity(intent);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if (AppConstants.isPackageInstalled("best.videoplayer.proplayer", context.getPackageManager())) {
                    intent.setPackage("best.videoplayer.proplayer");
                    intent.setDataAndType(Uri.parse(whatsappStatusModel.getPath()), "video/*");
                    context.startActivity(intent);
                } else {
                    View view = LayoutInflater.from(context).inflate(R.layout.app_install_dialog, null);
                    AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.Widget_Video_DialogTheme)
                            .setView(view);
                    AlertDialog alertDialog = builder.create();
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                    alertDialog.setOnKeyListener((dialog, keyCode, event) -> {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            dialog.cancel();
                            intent.setDataAndType(Uri.parse(whatsappStatusModel.getPath()), "video/*");
                            context.startActivity(intent);
                            return true;
                        }
                        return false;
                    });
                    view.findViewById(R.id.btn_okay).setOnClickListener(view1 -> {
                        alertDialog.dismiss();
                        try {
                            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=best.videoplayer.proplayer");
                            Intent intentPlayStore = new Intent(Intent.ACTION_VIEW, uri);
                            intentPlayStore.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intentPlayStore);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    view.findViewById(R.id.btn_no).setOnClickListener(view1 -> {
                        alertDialog.dismiss();
                        intent.setDataAndType(Uri.parse(whatsappStatusModel.getPath()), "video/*");
                        context.startActivity(intent);
                    });
                }
            } else {
                Intent intent = new Intent(context, View_MultipleImage_Activity.class);
                intent.putExtra("pos", position);
                intent.putExtra("isDownloaded", file.exists());
                context.startActivity(intent);
            }
        });

        viewHolder.tv_download.setOnClickListener(view -> {
            if (!file.exists()) {
                AppUtils.createFileFolder();
                String path = whatsappStatusModel.getPath();
                String substring = path.substring(path.lastIndexOf("/") + 1);
                try {
                    FileUtils.copyFileToDirectory(new File(path), new File(SaveFilePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String substring2 = substring.substring(12);
                refreshGallery(file.getPath());
                Toast.makeText(context, context.getResources().getString(R.string.saved_to) + SaveFilePath + substring2, Toast.LENGTH_SHORT).show();
                viewHolder.tv_download.setText("Downloaded");
                viewHolder.popup.setVisibility(View.VISIBLE);
            } else {
                Toast.makeText(context, "Already Downloaded", Toast.LENGTH_SHORT).show();
            }
        });

        viewHolder.popup.setOnClickListener(v -> {
            PopupMenu popupMenu = new PopupMenu(context, v);
            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()) {
                    case R.id.share:
                        Intent intentShare = new Intent("android.intent.action.SEND");
                        intentShare.setType("image/*");
                        intentShare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        Uri uri = FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file);
                        intentShare.putExtra(Intent.EXTRA_STREAM, uri);
                        context.startActivity(Intent.createChooser(intentShare, "Share"));
                        return true;
                    case R.id.delete:
                        File fdelete = new File(String.valueOf(file));
                        if (fdelete.exists()) {
                            if (fdelete.delete()) {
                                viewHolder.tv_download.setText("Download");
                                notifyDataSetChanged();
                                Toast.makeText(context, "Deleted", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Delete Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                        return true;
                    default:
                        return true;
                }
            });
            popupMenu.getMenu().add(1, R.id.share, 1, "Share");
            popupMenu.getMenu().add(1, R.id.delete, 2, "Delete");
            popupMenu.show();
        });
    }

    private void refreshGallery(String path) {
        MediaScannerConnection.scanFile(context, new String[]{new File(path).getAbsolutePath()}, new String[]{
                path.endsWith(".mp4") ? "video/*" : "image/*"}, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
            }

            public void onScanCompleted(String str, Uri uri) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return fileArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rl_main;
        ImageView pcw, iv_play, popup;
        TextView tv_download;

        public ViewHolder(View itemsWhatsappViewBinding) {
            super(itemsWhatsappViewBinding);
            popup = itemsWhatsappViewBinding.findViewById(R.id.popup);
            tv_download = itemsWhatsappViewBinding.findViewById(R.id.tv_download);
            rl_main = itemsWhatsappViewBinding.findViewById(R.id.rl_main);
            pcw = itemsWhatsappViewBinding.findViewById(R.id.pcw);
            iv_play = itemsWhatsappViewBinding.findViewById(R.id.iv_play);
        }
    }
}
