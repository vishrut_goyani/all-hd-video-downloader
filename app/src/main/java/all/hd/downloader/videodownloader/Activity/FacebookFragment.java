package all.hd.downloader.videodownloader.Activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import all.hd.downloader.videodownloader.Api.CommonClassForAPI;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;

public class FacebookFragment extends Fragment {
    private String VideoUrl;
    Context activity;
    private ClipboardManager clipBoard;
    CommonClassForAPI commonClassForAPI;
    MaterialButton LLOpenFacebbook;
    EditText et_text;
    TextView login_btn1, tv_paste;

    private UnifiedNativeAd mNativeAdDialog;
    View viewDialog;
    FrameLayout adFrame;
    AlertDialog.Builder builder;
    RoundedHorizontalProgressBar rh_progress_bar;
    TextView txtPerc;
    AlertDialog alertDialog;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facebook_main, container, false);
        init(view);
        this.commonClassForAPI = CommonClassForAPI.getInstance(getActivity());
        AppUtils.createFileFolder();
        initViews();
        if (adFrame != null)
            if (PreferenceUtility.getInstance(activity).getAdmobNative().equals("yes"))
                refreshDialogAd(adFrame);
        return view;
    }

    private void init(View view) {
        if (viewDialog == null)
            viewDialog = LayoutInflater.from(activity).inflate(R.layout.download_dialog, null);
        adFrame = viewDialog.findViewById(R.id.adFrame);
        rh_progress_bar = viewDialog.findViewById(R.id.rh_progress_bar);
        txtPerc = viewDialog.findViewById(R.id.txtPerc);

        login_btn1 = view.findViewById(R.id.login_btn1);
        tv_paste = view.findViewById(R.id.tv_paste);
        LLOpenFacebbook = view.findViewById(R.id.LLOpenFacebbook);
        et_text = view.findViewById(R.id.et_text);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.clipBoard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        PasteText();
    }

    private void initViews() {
        this.clipBoard = (ClipboardManager) this.activity.getSystemService(Context.CLIPBOARD_SERVICE);
        login_btn1.setOnClickListener(view -> initViewsFbActivity(view));
        tv_paste.setOnClickListener(view -> initViews1FbActivity(view));
        LLOpenFacebbook.setOnClickListener(view -> initViews2FbActivity(view));
    }

    public void initViewsFbActivity(View view) {
        String obj = et_text.getText().toString();
        if (obj.equals("")) {
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_url));
        } else if (!Patterns.WEB_URL.matcher(obj).matches()) {
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_valid_url));
        } else {
            GetFacebookData();
        }
    }

    public void initViews1FbActivity(View view) {
        PasteText();
    }

    public void initViews2FbActivity(View view) {
        AppUtils.OpenApp(this.activity, "com.facebook.katana");
    }

    private void GetFacebookData() {
        try {
            AppUtils.createFileFolder();
            String host = new URL(et_text.getText().toString()).getHost();
            Log.e("initViews: ", host);
            if (host.contains("facebook.com")) {
                AppUtils.showProgressDialog(getActivity());
                new callGetFacebookData().execute(et_text.getText().toString());
                return;
            }
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_valid_url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PasteText() {
        try {
            et_text.setText("");
            String stringExtra = getActivity().getIntent().getStringExtra("CopyIntent");
            if (stringExtra.equals("")) {
                if (this.clipBoard.hasPrimaryClip()) {
                    if (this.clipBoard.getPrimaryClipDescription().hasMimeType("text/plain")) {
                        ClipData.Item itemAt = this.clipBoard.getPrimaryClip().getItemAt(0);
                        if (itemAt.getText().toString().contains("facebook.com")) {
                            et_text.setText(itemAt.getText().toString());
                        }
                    } else if (this.clipBoard.getPrimaryClip().getItemAt(0).getText().toString().contains("facebook.com")) {
                        et_text.setText(this.clipBoard.getPrimaryClip().getItemAt(0).getText().toString());
                    }
                }
            } else if (stringExtra.contains("facebook.com")) {
                et_text.setText(stringExtra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class callGetFacebookData extends AsyncTask<String, Void, Document> {
        Document facebookDoc;

        callGetFacebookData() {
        }

        public void onPreExecute() {
            super.onPreExecute();
        }

        public Document doInBackground(String... strArr) {
            try {
                facebookDoc = Jsoup.connect(et_text.getText().toString()).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return facebookDoc;
        }

        public void onPostExecute(Document document) {
            AppUtils.hideProgressDialog(getActivity());
            try {
                VideoUrl = document.select("meta[property=\"og:video\"]").last().attr(FirebaseAnalytics.Param.CONTENT);
                if (!VideoUrl.equals("")) {
                    try {
                        download_data(VideoUrl, String.valueOf(AppUtils.RootDirectoryFacebookShow), getFilenameFromURL(VideoUrl));
                        VideoUrl = "";
                        et_text.setText("");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            } catch (NullPointerException e2) {
                e2.printStackTrace();
            }
        }
    }

    int downloadId;

    public void download_data(String url, String dirPath, String fileName) {
        downloadId = PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(() -> {
                    builder = new AlertDialog.Builder(activity, R.style.Downloader_DialogTheme)
                            .setView(viewDialog)
                            .setCancelable(true);
                    alertDialog = builder.create();
                    alertDialog.show();
                    rh_progress_bar.setMax(100);

                    alertDialog.setOnDismissListener(dialog -> PRDownloader.cancel(downloadId));
                })
                .setOnProgressListener(progress -> {
                    double perc = ((progress.currentBytes / (double) progress.totalBytes) * 100.0f);
                    rh_progress_bar.setProgress((int) perc);
                    txtPerc.setText((int) perc + " %");
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        Toast.makeText(activity, "Download Complete", Toast.LENGTH_SHORT).show();
                        refreshGallery(dirPath + "/" + fileName);
                        alertDialog.dismiss();
                    }

                    @Override
                    public void onError(Error error) {
                        Toast.makeText(activity, "Download Failed", Toast.LENGTH_SHORT).show();
                        alertDialog.dismiss();
                    }
                });
    }

    private void refreshGallery(String path) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{new File(path).getAbsolutePath()}, new String[]{
                path.endsWith(".mp4") ? "video/*" : "image/*"}, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
            }

            public void onScanCompleted(String str, Uri uri) {
            }
        });
    }

    public String getFilenameFromURL(String str) {
        try {
            return new File(new URL(str).getPath()).getName() + ".mp4";
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return System.currentTimeMillis() + ".mp4";
        }
    }

    public void refreshDialogAd(FrameLayout adFrame) {
        if (mNativeAdDialog != null) {
            mNativeAdDialog.destroy();
        }
        new AdLoader.Builder(activity, PreferenceUtility.getInstance(getActivity()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) getLayoutInflater().inflate(R.layout.admob_native, null);
            mNativeAdDialog = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdDialog, unifiedNativeAdView);
            adFrame.removeAllViews();
            adFrame.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
//        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
//        if (unifiedNativeAd.getStarRating() == null) {
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
//        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }
}
