package all.hd.downloader.videodownloader;

import android.app.Application;
import android.util.Log;

import androidx.appcompat.app.AppCompatDelegate;

import com.google.android.gms.ads.MobileAds;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import all.hd.downloader.videodownloader.utils.PreferenceUtility;

public class AllDownloaderApplication extends Application {
    private FirebaseRemoteConfig firebaseRemoteConfig;

    private static final String KEY_ADMOB_INTERSTITIAL = "admob_interstitial";
    private static final String KEY_ADMOB_BANNER = "admob_banner";
    private static final String KEY_ADMOB_NATIVE = "admob_native";
    private static final String KEY_ADMOB_OPEN_APP = "admob_open_app";
    private static final String KEY_ADM_APP_ID = "adm_app_id_all_video";
    private static final String KEY_ADM_OPEN_APP = "adm_open_app_all_video";
    private static final String KEY_ADM_NATIVE = "adm_native_all_video";
    private static final String KEY_ADM_INTERSTITAL = "adm_interstitial_all_video";
    private static final String KEY_ADM_BANNER = "adm_banner_all_video";

    static AllDownloaderApplication mInstance;

    public static AllDownloaderApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        FirebaseApp.initializeApp(mInstance);
        initPersonalAds();
    }

    private void initPersonalAds() {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings.Builder configBuilder = new FirebaseRemoteConfigSettings.Builder();
        long cacheInterval = 0;
        configBuilder.setMinimumFetchIntervalInSeconds(cacheInterval);
        firebaseRemoteConfig.setConfigSettingsAsync(configBuilder.build());
        firebaseRemoteConfig.setDefaults(R.xml.remote_config);

        firebaseRemoteConfig.fetchAndActivate()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        boolean updated = task.getResult();
                        Log.e("TAG", "Config params updated: " + updated);
                        Log.e("TAG", "fetched successfully");

                        PreferenceUtility.getInstance(getApplicationContext()).setAdmobInterstitial(firebaseRemoteConfig.getString(KEY_ADMOB_INTERSTITIAL));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdmobBanner(firebaseRemoteConfig.getString(KEY_ADMOB_BANNER));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdmobNative(firebaseRemoteConfig.getString(KEY_ADMOB_NATIVE));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdmobOpenApp(firebaseRemoteConfig.getString(KEY_ADMOB_OPEN_APP));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdm_native(firebaseRemoteConfig.getString(KEY_ADM_NATIVE));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdm_interstitial(firebaseRemoteConfig.getString(KEY_ADM_INTERSTITAL));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdm_banner(firebaseRemoteConfig.getString(KEY_ADM_BANNER));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdm_open_app(firebaseRemoteConfig.getString(KEY_ADM_OPEN_APP));
                        PreferenceUtility.getInstance(getApplicationContext()).setAdm_app_id(firebaseRemoteConfig.getString(KEY_ADM_APP_ID));
                        MobileAds.initialize(this, PreferenceUtility.getInstance(getApplicationContext()).getAdm_app_id());
                        Log.e("TAG", "app id : " + PreferenceUtility.getInstance(getApplicationContext()).getAdm_app_id());
                        Log.e("TAG", "app id : " + PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
                        Log.e("TAG", "app id : " + PreferenceUtility.getInstance(getApplicationContext()).getAdm_native());
                        Log.e("TAG", "app id : " + PreferenceUtility.getInstance(getApplicationContext()).getAdm_banner());
                    } else {
                        Log.e("TAG", "Fetch failed");
                    }
                });
    }

}