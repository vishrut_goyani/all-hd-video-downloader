package all.hd.downloader.videodownloader.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;

import java.io.File;
import java.util.ArrayList;

import all.hd.downloader.videodownloader.Activity.ALLCreationGalleryActivity;
import all.hd.downloader.videodownloader.Activity.FullViewActivity;
import all.hd.downloader.videodownloader.Adapter.FileListAdapter;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.Interfaces.FileListClickInterface;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;

public class FBDownloadedFragment extends Fragment implements FileListClickInterface {
    private ALLCreationGalleryActivity activity;
    private ArrayList<File> fileArrayList;
    private FileListAdapter fileListAdapter;
    TextView tv_NoResult;
    SwipeRefreshLayout swiperefresh;
    RecyclerView rv_fileList;

    private com.google.android.gms.ads.InterstitialAd interstitialAd;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (ALLCreationGalleryActivity) context;
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            getArguments().getString("m");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        this.activity = (ALLCreationGalleryActivity) getActivity();
        getAllFiles();
        if (AppUtils.getInstance(getActivity()).isNetworkAvailable(getActivity()))
            if (PreferenceUtility.getInstance(getActivity()).getAdmobInterstitial().equals("yes"))
                loadInterstitialAdmob();
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View view = layoutInflater.inflate(R.layout.fragment_history, viewGroup, false);
        init(view);
        initViews();
        return view;
    }

    private void init(View view) {
        tv_NoResult = view.findViewById(R.id.tv_NoResult);
        swiperefresh = view.findViewById(R.id.swiperefresh);
        rv_fileList = view.findViewById(R.id.rv_fileList);
        tv_NoResult = view.findViewById(R.id.tv_NoResult);
    }

    private void initViews() {
        swiperefresh.setOnRefreshListener(() -> initViewsInFBDownloadedFragment());
    }

    public void initViewsInFBDownloadedFragment() {
        getAllFiles();
        swiperefresh.setRefreshing(false);
    }

    private void getAllFiles() {
        this.fileArrayList = new ArrayList<>();
        File[] listFiles = AppUtils.RootDirectoryFacebookShow.listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                this.fileArrayList.add(file);
            }
            if (fileArrayList.size() != 0) {
                tv_NoResult.setVisibility(View.GONE);
            } else {
                tv_NoResult.setVisibility(View.VISIBLE);
            }
            this.fileListAdapter = new FileListAdapter(this.activity, this.fileArrayList, this);
            rv_fileList.setAdapter(this.fileListAdapter);
        }
    }

    @Override
    public void getPosition(int i, File file) {
        Intent intent = new Intent(this.activity, FullViewActivity.class);
        intent.putExtra("ImageDataFile", this.fileArrayList);
        intent.putExtra("Position", i);
        activity.startActivity(intent);
        showInterstitialAdAdmob();
    }

    public void loadInterstitialAdmob() {
        interstitialAd = new com.google.android.gms.ads.InterstitialAd(getActivity());
        interstitialAd.setAdUnitId(PreferenceUtility.getInstance(getActivity()).getAdm_interstitial());
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
