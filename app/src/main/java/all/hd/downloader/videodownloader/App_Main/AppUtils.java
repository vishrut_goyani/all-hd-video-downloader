package all.hd.downloader.videodownloader.App_Main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import all.hd.downloader.videodownloader.R;

public class AppUtils {
    public static String RECEIVER_ADDRESS = "andro.ops151@gmail.com";
    public static String RootDirectoryFacebook = "All Video HD Downloader/Facebook/";
    public static File RootDirectoryFacebookShow = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/All Video HD Downloader/Facebook");
    public static String RootDirectoryInsta = "All Video HD Downloader/Insta/";
    public static File RootDirectoryInstaShow = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/All Video HD Downloader/Insta/");
    public static String RootDirectoryTwitter = "All Video HD Downloader/Twitter/";
    public static File RootDirectoryTwitterShow = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/All Video HD Downloader/Twitter");
    public static File RootDirectoryWhatsappShow = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/All Video HD Downloader/Whatsapp");
    private static Context context;
    public static Dialog customDialog;
    public static final String is_rated = "is_rated";
    public static final String count_ = "counts";
    public static final String device_token = "device_token";
    public static final String APP_PREFS = "DOWNLOADER_PREFS";
    static List<Rect> exclusionRects = new ArrayList<>();
    static SharedPreferences sharedPreferences;
    private static AppUtils mInstance;
    public static Context mContext;

    public AppUtils(Context context) {
        mContext = context;
        try {
            sharedPreferences = mContext.getSharedPreferences(APP_PREFS, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized AppUtils getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new AppUtils(context);
        }
        return mInstance;
    }

    public static boolean isDarkMode(AppCompatActivity activity) {
        return (activity.getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK) == Configuration.UI_MODE_NIGHT_YES;
    }

    public static void setToast(Context context2, String str) {
        Toast makeText = Toast.makeText(context2, str, Toast.LENGTH_SHORT);
        makeText.setGravity(17, 0, 0);
        makeText.show();
    }

    public static void createFileFolder() {
        if (!RootDirectoryFacebookShow.exists()) {
            RootDirectoryFacebookShow.mkdirs();
        }
        if (!RootDirectoryInstaShow.exists()) {
            RootDirectoryInstaShow.mkdirs();
        }
        if (!RootDirectoryTwitterShow.exists()) {
            RootDirectoryTwitterShow.mkdirs();
        }
        if (!RootDirectoryWhatsappShow.exists()) {
            RootDirectoryWhatsappShow.mkdirs();
        }
    }

    public static void showProgressDialog(Activity activity) {
        Dialog dialog = customDialog;
        if (dialog != null) {
            dialog.dismiss();
            customDialog = null;
        }
        customDialog = new Dialog(activity, R.style.Downloader_DialogTheme);
        View inflate = LayoutInflater.from(activity).inflate(R.layout.progress_dialog, (ViewGroup) null);
        customDialog.setCancelable(false);
        customDialog.setContentView(inflate);
        if (!customDialog.isShowing() && !activity.isFinishing()) {
            customDialog.show();
        }
    }

    public static void hideProgressDialog(Activity activity) {
        Dialog dialog = customDialog;
        if (dialog != null && dialog.isShowing()) {
            customDialog.dismiss();
        }
    }

    public boolean isNetworkAvailable(Context mContext) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void shareImage(Context context2, String str) {
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.STREAM", Uri.parse(MediaStore.Images.Media.insertImage(context2.getContentResolver(), str, "", (String) null)));
            intent.setType("image/*");
            context2.startActivity(Intent.createChooser(intent, context2.getResources().getString(R.string.share_image_to)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("WrongConstant")
    public static void shareImageVideoOnWhatsapp(Context context2, String str, boolean z) {
        Uri parse = Uri.parse(str);
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.setPackage("com.whatsapp");
        intent.putExtra("android.intent.extra.TEXT", "");
        intent.putExtra("android.intent.extra.STREAM", parse);
        if (z) {
            intent.setType("video/*");
        } else {
            intent.setType("image/*");
        }
        intent.addFlags(1);
        try {
            context2.startActivity(intent);
        } catch (Exception unused) {
            setToast(context2, context2.getResources().getString(R.string.whatsapp_not_installed));
        }
    }

    @SuppressLint("WrongConstant")
    public static void shareVideo(Context context2, String str) {
        Uri parse = Uri.parse(str);
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("video/mp4");
        intent.putExtra("android.intent.extra.STREAM", parse);
        intent.addFlags(1);
        try {
            context2.startActivity(Intent.createChooser(intent, "Share Video using"));
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(context2, context2.getResources().getString(R.string.no_app_installed), Toast.LENGTH_LONG).show();
        }
    }

    public static void ShareApp(Context context2) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.SUBJECT", context2.getString(R.string.app_name));
        intent.putExtra("android.intent.extra.TEXT", context2.getString(R.string.share_app_desc) + ("\nhttps://play.google.com/store/apps/details?id=" + context2.getPackageName()));
        intent.setType("text/plain");
        context2.startActivity(Intent.createChooser(intent, "Share"));
    }

    public static void OpenApp(Context context2, String str) {
        Intent launchIntentForPackage = context2.getPackageManager().getLaunchIntentForPackage(str);
        if (launchIntentForPackage != null) {
            context2.startActivity(launchIntentForPackage);
        } else {
            setToast(context2, context2.getResources().getString(R.string.app_not_available));
        }
    }

    public static boolean isNullOrEmpty(String str) {
        return str == null || str.length() == 0 || str.equalsIgnoreCase("null") || str.equalsIgnoreCase("0");
    }

    public static void updateGestureExclusionLeft(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT < 29) return;
        exclusionRects.clear();
        Rect rect = new Rect(0, 0, dpToPx(activity, 16), (int) (getScreenHeight(activity) / 1.6));
        exclusionRects.add(rect);
        activity.findViewById(android.R.id.content).setSystemGestureExclusionRects(exclusionRects);
    }

    public static int getScreenHeight(AppCompatActivity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        return height;
    }

    public static int dpToPx(Context context, int i) {
        return (int) (((float) i) * context.getResources().getDisplayMetrics().density);
    }

    public static void setRated(boolean rated) {
        sharedPreferences.edit().putBoolean(is_rated, rated).apply();
    }

    public static boolean getRated() {
        return sharedPreferences.getBoolean(is_rated, false);
    }

    public static void setCount(int count) {
        sharedPreferences.edit().putInt(count_, count).apply();
    }

    public static int getCounts() {
        return sharedPreferences.getInt(count_, 0);
    }

    public static void setDeviceToken(String deviceToken) {
        sharedPreferences.edit().putString(device_token, deviceToken).apply();
    }

    public static String getDeviceToken() {
        return sharedPreferences.getString(device_token, null);
    }
}
