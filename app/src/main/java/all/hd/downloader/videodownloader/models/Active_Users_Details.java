package all.hd.downloader.videodownloader.models;

public class Active_Users_Details {
    String date;

    public Active_Users_Details(String date) {
        this.date = date;
    }

    public Active_Users_Details() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
