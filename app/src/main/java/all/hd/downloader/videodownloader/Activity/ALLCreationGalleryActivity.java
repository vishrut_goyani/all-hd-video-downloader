package all.hd.downloader.videodownloader.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.Fragment.FBDownloadedFragment;
import all.hd.downloader.videodownloader.Fragment.InstaDownloadedFragment;
import all.hd.downloader.videodownloader.Fragment.TwitterDownloadedFragment;
import all.hd.downloader.videodownloader.Fragment.WhatsAppDowndlededFragment;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;
import nl.joery.animatedbottombar.AnimatedBottomBar;

public class ALLCreationGalleryActivity extends AppCompatActivity {
    ALLCreationGalleryActivity activity;
    ViewPager viewpager;
    AnimatedBottomBar bottomBar;
    Toolbar toolbar;

    private RelativeLayout banner_container;
    private com.google.android.gms.ads.AdView adm_banner;

    private com.google.android.gms.ads.InterstitialAd interstitialAd;

    UnifiedNativeAd mNativeAdSmall;
    FrameLayout adFrameSmall;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_gallery);
        this.activity = this;
        init();
        initViews();

        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            if (PreferenceUtility.getInstance(ALLCreationGalleryActivity.this).getAdmobBanner().equals("yes"))
                loadBannerAdmob();

        if (PreferenceUtility.getInstance(ALLCreationGalleryActivity.this).getAdmobNative().equals("yes"))
            refreshAdSmall(adFrameSmall);
    }

    public void init() {
        adFrameSmall = findViewById(R.id.adFrameSmall);

        toolbar = findViewById(R.id.toolbar);
        bottomBar = findViewById(R.id.bottomBar);
        viewpager = findViewById(R.id.viewpager);
        banner_container = findViewById(R.id.banner_container);

        adm_banner = new com.google.android.gms.ads.AdView(this);
        com.google.android.gms.ads.AdSize adSize = getAdSize();
        adm_banner.setAdSize(adSize);
        adm_banner.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_banner());

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private com.google.android.gms.ads.AdSize getAdSize() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);
        return com.google.android.gms.ads.AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (adm_banner != null)
            adm_banner.resume();

        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            if (PreferenceUtility.getInstance(ALLCreationGalleryActivity.this).getAdmobInterstitial().equals("yes"))
                loadInterstitialAdmob();
    }

    public void loadInterstitialAdmob() {
        interstitialAd = new com.google.android.gms.ads.InterstitialAd(this);
        interstitialAd.setAdUnitId(PreferenceUtility.getInstance(getApplicationContext()).getAdm_interstitial());
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }

            @Override
            public void onAdOpened() {

            }

            @Override
            public void onAdLeftApplication() {

            }

            @Override
            public void onAdClosed() {
            }

            @Override
            public void onAdClicked() {

            }
        });
    }

    public void showInterstitialAdAdmob() {
        try {
            if (interstitialAd != null && interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadBannerAdmob() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adm_banner.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
            }
        });
        adm_banner.loadAd(adRequest);
        banner_container.removeAllViews();
        banner_container.addView(adm_banner);
    }

    public void initViews() {
        setupViewPager(viewpager);
        bottomBar.setOnTabSelected(tab -> {
            switch (tab.getId()) {
                case R.id.bottom_instagram:
                    viewpager.setCurrentItem(0);
                    runOnUiThread(() -> {
                        bottomBar.setTabColorSelected(getResources().getColor(R.color.instagram_accent));
                        bottomBar.setIndicatorColor(getResources().getColor(R.color.instagram_accent));
                    });
                    break;
                case R.id.bottom_whatsapp:
                    viewpager.setCurrentItem(1);
                    runOnUiThread(() -> {
                        bottomBar.setTabColorSelected(getResources().getColor(R.color.whatsapp_accent));
                        bottomBar.setIndicatorColor(getResources().getColor(R.color.whatsapp_accent));
                    });
                    break;
                case R.id.bottom_facebook:
                    viewpager.setCurrentItem(2);
                    runOnUiThread(() -> {
                        bottomBar.setTabColorSelected(getResources().getColor(R.color.facebook_accent));
                        bottomBar.setIndicatorColor(getResources().getColor(R.color.facebook_accent));
                    });
                    break;
                case R.id.bottom_twitter:
                    viewpager.setCurrentItem(3);
                    runOnUiThread(() -> {
                        bottomBar.setTabColorSelected(getResources().getColor(R.color.twitter_accent));
                        bottomBar.setIndicatorColor(getResources().getColor(R.color.twitter_accent));
                    });
                    break;
            }
            return null;
        });

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                bottomBar.selectTabAt(position, true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        AppUtils.createFileFolder();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this.activity.getSupportFragmentManager(), 1);
        viewPagerAdapter.addFragment(new InstaDownloadedFragment(), "Instagram");
        viewPagerAdapter.addFragment(new WhatsAppDowndlededFragment(), "Whatsapp");
        viewPagerAdapter.addFragment(new FBDownloadedFragment(), "Facebook");
        viewPagerAdapter.addFragment(new TwitterDownloadedFragment(), "Twitter");
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(4);
    }

    public class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList();
        private final List<String> mFragmentTitleList = new ArrayList();

        ViewPagerAdapter(FragmentManager fragmentManager, int i) {
            super(fragmentManager, i);
        }

        @Override
        public Fragment getItem(int i) {
            return this.mFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return this.mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String str) {
            this.mFragmentList.add(fragment);
            this.mFragmentTitleList.add(str);
        }

        @Override
        public CharSequence getPageTitle(int i) {
            return this.mFragmentTitleList.get(i);
        }
    }

    @Override
    public void onBackPressed() {
        if (AppUtils.getInstance(this).isNetworkAvailable(this))
            showInterstitialAdAdmob();
        finish();
    }

    public void refreshAdSmall(FrameLayout adFrameSmall) {
        if (mNativeAdSmall != null) {
            mNativeAdSmall.destroy();
        }
        new AdLoader.Builder(this, PreferenceUtility.getInstance(ALLCreationGalleryActivity.this).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) LayoutInflater.from(ALLCreationGalleryActivity.this).inflate(R.layout.admob_native_small, null);
            mNativeAdSmall = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdSmall, unifiedNativeAdView);
            adFrameSmall.removeAllViews();
            adFrameSmall.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getStarRating() == null) {
            unifiedNativeAdView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }
}
