package all.hd.downloader.videodownloader.Fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.downloader.Error;
import com.downloader.OnDownloadListener;
import com.downloader.PRDownloader;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.google.android.material.button.MaterialButton;
import com.sasank.roundedhorizontalprogress.RoundedHorizontalProgressBar;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import all.hd.downloader.videodownloader.Api.CommonClassForAPI;
import all.hd.downloader.videodownloader.App_Main.AppUtils;
import all.hd.downloader.videodownloader.R;
import all.hd.downloader.videodownloader.models.TwitterResponse;
import all.hd.downloader.videodownloader.utils.PreferenceUtility;
import io.reactivex.observers.DisposableObserver;

public class TwitterFragment extends Fragment {
    private String VideoUrl;
    Context activity;
    private ClipboardManager clipBoard;
    CommonClassForAPI commonClassForAPI;
    EditText et_text;
    TextView tv_paste, login_btn1;
    MaterialButton LLOpenTwitter;

    private UnifiedNativeAd mNativeAdDialog;
    View viewDialog;
    FrameLayout adFrame;
    AlertDialog.Builder builder;
    RoundedHorizontalProgressBar rh_progress_bar;
    TextView txtPerc;
    AlertDialog alertDialog;

    private DisposableObserver<TwitterResponse> observer = new DisposableObserver<TwitterResponse>() {
        public void onNext(TwitterResponse twitterResponse) {
            AppUtils.hideProgressDialog(getActivity());
            try {
                VideoUrl = twitterResponse.getVideos().get(0).getUrl();
                if (twitterResponse.getVideos().get(0).getType().equals("image")) {
                    download_data(VideoUrl, String.valueOf(AppUtils.RootDirectoryTwitterShow), getFilenameFromURL(VideoUrl, "image"));
                    et_text.setText("");
                    return;
                }
                VideoUrl = twitterResponse.getVideos().get(twitterResponse.getVideos().size() - 1).getUrl();
                download_data(VideoUrl, String.valueOf(AppUtils.RootDirectoryTwitterShow), getFilenameFromURL(VideoUrl, "mp4"));
                et_text.setText("");
            } catch (Exception e) {
                e.printStackTrace();
                AppUtils.setToast(getActivity(), getResources().getString(R.string.no_media_on_tweet));
            }
        }

        @Override
        public void onError(Throwable th) {
            AppUtils.hideProgressDialog(getActivity());
            th.printStackTrace();
        }

        @Override
        public void onComplete() {
            AppUtils.hideProgressDialog(getActivity());
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = context;
    }

    public void download_data(String url, String dirPath, String fileName) {
        PRDownloader.download(url, dirPath, fileName)
                .build()
                .setOnStartOrResumeListener(() -> {
                    builder = new AlertDialog.Builder(activity, R.style.Downloader_DialogTheme)
                            .setView(viewDialog)
                            .setCancelable(true);
                    alertDialog = builder.create();
                    alertDialog.show();
                    rh_progress_bar.setMax(100);

                })
                .setOnPauseListener(() -> {

                })
                .setOnCancelListener(() -> {

                })
                .setOnProgressListener(progress -> {
                    double perc = ((progress.currentBytes / (double) progress.totalBytes) * 100.0f);
                    rh_progress_bar.setProgress((int) perc);
                    txtPerc.setText((int) perc + " %");
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        alertDialog.dismiss();
                        Toast.makeText(activity, "Download Complete", Toast.LENGTH_SHORT).show();
                        refreshGallery(dirPath + "/" + fileName);
                    }

                    @Override
                    public void onError(Error error) {
                        alertDialog.dismiss();
                        Toast.makeText(activity, "Download Failed", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void refreshGallery(String path) {
        MediaScannerConnection.scanFile(getActivity(), new String[]{new File(path).getAbsolutePath()}, new String[]{
                path.endsWith(".mp4") ? "video/*" : "image/*"}, new MediaScannerConnection.MediaScannerConnectionClient() {
            public void onMediaScannerConnected() {
            }

            public void onScanCompleted(String str, Uri uri) {
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_twitter_main, container, false);
        this.commonClassForAPI = CommonClassForAPI.getInstance(getActivity());
        init(view);
        AppUtils.createFileFolder();
        initViews();
        return view;
    }

    private void init(View view) {
        if (viewDialog == null)
            viewDialog = LayoutInflater.from(activity).inflate(R.layout.download_dialog, null);
        adFrame = viewDialog.findViewById(R.id.adFrame);
        rh_progress_bar = viewDialog.findViewById(R.id.rh_progress_bar);
        txtPerc = viewDialog.findViewById(R.id.txtPerc);

        LLOpenTwitter = view.findViewById(R.id.LLOpenTwitter);
        et_text = view.findViewById(R.id.et_text);
        login_btn1 = view.findViewById(R.id.login_btn1);
        tv_paste = view.findViewById(R.id.tv_paste);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.clipBoard = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
        PasteText();
        if (adFrame != null)
            if (PreferenceUtility.getInstance(activity).getAdmobNative().equals("yes"))
                refreshDialogAd(adFrame);
    }

    private void initViews() {
        this.clipBoard = (ClipboardManager) this.activity.getSystemService(Context.CLIPBOARD_SERVICE);
        login_btn1.setOnClickListener(view -> initViewsTwitterActivity(view));
        tv_paste.setOnClickListener(view -> PasteText());
        LLOpenTwitter.setOnClickListener(view -> AppUtils.OpenApp(this.activity, "com.twitter.android"));
    }

    public void initViewsTwitterActivity(View view) {
        String obj = et_text.getText().toString();
        if (obj.equals("")) {
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_url));
        } else if (!Patterns.WEB_URL.matcher(obj).matches()) {
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_valid_url));
        } else {
            AppUtils.showProgressDialog(getActivity());
            GetTwitterData();
        }
    }

    private void GetTwitterData() {
        try {
            AppUtils.createFileFolder();
            if (new URL(et_text.getText().toString()).getHost().contains("twitter.com")) {
                Long tweetId = getTweetId(et_text.getText().toString());
                if (tweetId != null) {
                    callGetTwitterData(String.valueOf(tweetId));
                    return;
                }
                return;
            }
            AppUtils.setToast(this.activity, getResources().getString(R.string.enter_url));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Long getTweetId(String str) {
        try {
            return Long.valueOf(Long.parseLong(str.split("\\/")[5].split("\\?")[0]));
        } catch (Exception e) {
            Log.d("TAG", "getTweetId: " + e.getLocalizedMessage());
            return null;
        }
    }

    private void PasteText() {
        try {
            et_text.setText("");
            String stringExtra = getActivity().getIntent().getStringExtra("CopyIntent");
            if (stringExtra.equals("")) {
                if (this.clipBoard.hasPrimaryClip()) {
                    if (this.clipBoard.getPrimaryClipDescription().hasMimeType("text/plain")) {
                        ClipData.Item itemAt = this.clipBoard.getPrimaryClip().getItemAt(0);
                        if (itemAt.getText().toString().contains("twitter.com")) {
                            et_text.setText(itemAt.getText().toString());
                        }
                    } else if (this.clipBoard.getPrimaryClip().getItemAt(0).getText().toString().contains("twitter.com")) {
                        et_text.setText(this.clipBoard.getPrimaryClip().getItemAt(0).getText().toString());
                    }
                }
            } else if (stringExtra.contains("twitter.com")) {
                et_text.setText(stringExtra);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callGetTwitterData(String str) {
        try {
            if (!new AppUtils(this.activity).isNetworkAvailable(getContext())) {
                AppUtils.setToast(this.activity, getResources().getString(R.string.no_net_conn));
            } else if (this.commonClassForAPI != null) {
                AppUtils.showProgressDialog(getActivity());
                this.commonClassForAPI.callTwitterApi(this.observer, "https://twittervideodownloaderpro.com/twittervideodownloadv2/index.php", str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getFilenameFromURL(String str, String str2) {
        if (str2.equals("image")) {
            try {
                return new File(new URL(str).getPath()).getName() + "";
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return System.currentTimeMillis() + ".jpg";
            }
        } else {
            try {
                return new File(new URL(str).getPath()).getName() + "";
            } catch (MalformedURLException e2) {
                e2.printStackTrace();
                return System.currentTimeMillis() + ".mp4";
            }
        }
    }

    public void refreshDialogAd(FrameLayout adFrame) {
        if (mNativeAdDialog != null) {
            mNativeAdDialog.destroy();
        }
        new AdLoader.Builder(activity, PreferenceUtility.getInstance(getActivity()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) getLayoutInflater().inflate(R.layout.admob_native, null);
            mNativeAdDialog = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdDialog, unifiedNativeAdView);
            adFrame.removeAllViews();
            adFrame.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                if (PreferenceUtility.getInstance(activity).getAdmobInterstitial().equals("yes"))
                    refreshDialogAdPersonal(adFrame);
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void refreshDialogAdPersonal(FrameLayout adFrame) {
        if (mNativeAdDialog != null) {
            mNativeAdDialog.destroy();
        }
        new AdLoader.Builder(activity, PreferenceUtility.getInstance(getActivity()).getAdm_native()).forUnifiedNativeAd(unifiedNativeAd -> {
            UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) getLayoutInflater().inflate(R.layout.admob_native, null);
            mNativeAdDialog = unifiedNativeAd;
            populateUnifiedNativeAdView(mNativeAdDialog, unifiedNativeAdView);
            adFrame.removeAllViews();
            adFrame.addView(unifiedNativeAdView);
        }).withAdListener(new AdListener() {
            public void onAdFailedToLoad(LoadAdError loadAdError) {
            }
        }).withNativeAdOptions(new NativeAdOptions.Builder().build()).build().loadAd(new AdRequest.Builder().build());
    }

    public void populateUnifiedNativeAdView(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView(unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_icon));
//        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(View.GONE);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.INVISIBLE);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(View.VISIBLE);
            ((MaterialButton) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(View.GONE);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(View.VISIBLE);
        }
//        if (unifiedNativeAd.getStarRating() == null) {
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.INVISIBLE);
//        } else {
//            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
//            unifiedNativeAdView.getStarRatingView().setVisibility(View.VISIBLE);
//        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(View.VISIBLE);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
    }
}
