package all.hd.downloader.videodownloader.Activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import all.hd.downloader.videodownloader.R;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

public class Video_View_Activity extends AppCompatActivity {
    UniversalVideoView videoView;
    String video_path;
    private UniversalMediaController mMediaController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_view_activity);
        videoView = findViewById(R.id.videoView);

        video_path = getIntent().getStringExtra("path");
        mMediaController = (UniversalMediaController) findViewById(R.id.media_controller);
        videoView.setVideoPath(video_path);
        videoView.setMediaController(mMediaController);
        videoView.start();
    }

    @Override
    public void onBackPressed() {
        if (videoView.isPlaying()) {
            videoView.pause();
        }
        super.onBackPressed();
    }
}
