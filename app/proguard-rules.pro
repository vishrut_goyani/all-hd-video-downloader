# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keep class com.downloader.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.downloader.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.bumptech.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.bumptech.** { *; }
# keep everything in this package from being removed or renamed
-keep class androidx.** { *; }

# keep everything in this package from being renamed only
-keepnames class androidx.** { *; }
# keep everything in this package from being removed or renamed
-keep class io.** { *; }

# keep everything in this package from being renamed only
-keepnames class io.** { *; }
# keep everything in this package from being removed or renamed
-keep class org.** { *; }

# keep everything in this package from being renamed only
-keepnames class org.** { *; }
# keep everything in this package from being removed or renamed
-keep class okhttp3.** { *; }

# keep everything in this package from being renamed only
-keepnames class okhttp3.** { *; }
# keep everything in this package from being removed or renamed
-keep class retrofit2.** { *; }

# keep everything in this package from being renamed only
-keepnames class retrofit2.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.example.allvideodownloader.Fragment.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.example.allvideodownloader.Fragment.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.example.allvideodownloader.Activity.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.example.allvideodownloader.Activity.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.example.allvideodownloader.Adapter.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.example.allvideodownloader.Adapter.** { *; }
# keep everything in this package from being removed or renamed
-keep class com.example.allvideodownloader.App_Main.** { *; }

# keep everything in this package from being renamed only
-keepnames class com.example.allvideodownloader.App_Main.** { *; }

# keep everything in this package from being removed or renamed
-keep class all.hd.downloader.videodownloader.models.** { *; }

# keep everything in this package from being renamed only
-keepnames class all.hd.downloader.videodownloader.models.** { *; }

-keepclassmembers,allowobfuscation class * {
  @com.google.gson.annotations.SerializedName <fields>;
}

-keep class com.google.ads.mediation.admob.AdMobAdapter {
 *;
}

-keep class com.google.ads.mediation.AdUrlAdapter {
 *;
}
